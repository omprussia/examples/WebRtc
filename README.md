# WebRTC Examples

Application in C++ demonstrates WebRTC audio/video call.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
to the Open Mobile Platform.

For information about contributors see [AUTHORS](AUTHORS.md).

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a common structure of an application based on C++ and QML for Aurora OS.

* [qml](qml) directory contains the QML source code and the UI resources.
  * [cover](qml/cover) directory contains the application cover implementations.
  * [icons](qml/icons) directory contains the custom UI icons.
  * [pages](qml/pages) directory contains the application pages.
  * [WebrtcExamples.qml](qml/WebrtcExamples.qml) file
    provides the application window implementation.
* [src](src) directory contains the C++ source code.
  * [main.cpp](src/main.cpp) file is the application entry point.
* [CMakeLists.txt](CMakeLists.txt) file
  describes the project structure for the CMake build system.
* [ru.auroraos.WebrtcExamples.desktop](ru.auroraos.WebrtcExamples.desktop) file
  defines the display and parameters for launching the application.
* [translations](translations) directory contains the UI translation files.
* [rpm](rpm) directory contains the rpm-package build settings.
  * [ru.auroraos.WebrtcExamples.spec](rpm/ru.auroraos.WebrtcExamples.spec) file is used by rpmbuild tool.
* [signaling_server](signaling_server) directory contains a test html page.
  * [server_test.html](signaling_server/server_test.html) is a test html page for checking
  the connection on a PC.

## Compatibility

The project is compatible with the Aurora OS, starting with version 5.0.0.60.

## Project Building

The project is built in the usual way using the Aurora SDK.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
