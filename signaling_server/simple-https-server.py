#!/usr/bin/python

from http.server import HTTPServer, SimpleHTTPRequestHandler
import ssl

# Create self signed certificate
# openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem

httpd = HTTPServer(('0.0.0.0', 8000), SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket (httpd.socket,
        keyfile="key.pem",
        certfile="cert.pem", server_side=True)

httpd.serve_forever()
