#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>

#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>

#include <list>
#include <vector>

#include "log.h"

namespace beast = boost::beast;                // from <boost/beast.hpp>
namespace http = boost::beast::http;           // from <boost/beast/http.hpp>
namespace websocket = boost::beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;                   // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;              // from <boost/asio/ip/tcp.hpp>

boost::asio::io_context &ioc_get()
{
    static boost::asio::io_context ioc;
    return ioc;
}

volatile sig_atomic_t client_id = 0;
volatile sig_atomic_t client_count = 0;

typedef websocket::stream<tcp::socket> ws_t;
typedef std::list<ws_t> ws_list_t;

ws_list_t ws_list;
std::mutex ws_list_mutex;
tcp::socket tcp_socket{ioc_get()};

void ws_thread_func(ws_t &ws, size_t id)
{
    try {
        // Set a decorator to change the Server of the handshake
        ws.set_option(websocket::stream_base::decorator([](websocket::response_type &res) {
            res.set(http::field::server, std::string(BOOST_BEAST_VERSION_STRING) + __FILE__);
        }));
        ws.text(true);
        ws.accept();

        while (ws.is_open()) {
            std::string str;
            auto buffer = boost::asio::dynamic_buffer(str);
            ws.read(buffer);
            LOG("%s", str.c_str());
            if (str == "{\"type\":\"disconnect\"}") {
                std::lock_guard<std::mutex> lock(ws_list_mutex);
                ws.close(websocket::close_code::normal);
                ws_list.remove_if(
                    [&ws](const ws_t &candidate_to_remove) { return &ws == &candidate_to_remove; });
                break;
            }

            if (tcp_socket.is_open()) {
                boost::asio::write(tcp_socket, boost::asio::buffer(str.c_str(), str.length() + 1));
                continue;
            }

            {
                std::lock_guard<std::mutex> lock(ws_list_mutex);
                auto ws_other = std::find_if(ws_list.begin(),
                                             ws_list.end(),
                                             [&ws](const ws_t &ws_other_cand) {
                                                 return ws_other_cand.is_open()
                                                        && &ws != &ws_other_cand;
                                             });

                if (ws_other == ws_list.end()) {
                    LOG("ws_other not found");
                    continue;
                }

                LOG("ws_other found");

                ws_other->write(buffer.data());
            }
        }
    } catch (const beast::system_error &se) {
        if (se.code() != websocket::error::closed)
            std::cerr << "Error: " << se.code().message() << std::endl;
    } catch (std::exception const &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
    client_count--;
    LOG("client_count: %d client_id: %d", client_count, client_id);
}

void tcp_sock_thread_func(std::string address_str, uint16_t port)
{
    net::ip::address address = net::ip::make_address("0.0.0.0");
    tcp::acceptor acceptor{ioc_get(), {address, port}};
    for (;;) {
        acceptor.accept(tcp_socket);
        client_count++;
        client_id++;
        LOG("client_count: %d client_id: %d", client_count, client_id);

        std::string str;
        while (tcp_socket.is_open()) {
            uint8_t byte[1];
            size_t byte_cnt = 0;
            try {
                byte_cnt = boost::asio::read(tcp_socket,
                                             boost::asio::buffer(byte, 1),
                                             boost::asio::transfer_all());
            } catch (const boost::system::system_error &err) {
                ERR("failed: read(): %d: %s", err.code().value(), err.code().message().c_str());
                tcp_socket.close();
                continue;
            } catch (std::exception &except) {
                LOG("exception: read(): %s", except.what());
                continue;
            }

            if (byte_cnt == 0)
                continue;

            if (byte[0] != '\0')
                str.push_back(byte[0]);
            else if (str.size() > 0) {
                LOG("\n\"%s\" ", str.c_str());
                std::lock_guard<std::mutex> lock(ws_list_mutex);
                auto ws_other = std::find_if(ws_list.begin(),
                                             ws_list.end(),
                                             [](const ws_t &ws_other_cand) {
                                                 return ws_other_cand.is_open();
                                             });

                if (ws_other == ws_list.end()) {
                    LOG("ws_other not found");
                    str = "";
                    continue;
                }
                LOG("ws_other found");

                ws_other->write(boost::asio::buffer(str.c_str(), strlen(str.c_str())));
                str = "";
            }
        }
        client_count--;
        LOG("client_count: %d client_id: %d", client_count, client_id);
    }
}

void usage()
{
    std::cerr << "Usage: contact_server <ws address> <ws port> <tcp address> <tcp port>\n"
              << "Example:\n"
              << "    contact_server 0.0.0.0 8888 0.0.0.0 8889\n";
}

int main(int argc, char *argv[])
{
    auto address = net::ip::make_address("0.0.0.0");
    uint16_t port = 8888;
    std::string tcp_address = "0.0.0.0";
    uint16_t tcp_port = 8889;

    if (argc > 1 && (std::string(argv[1]) == "--help" || std::string(argv[1]) == "-h")) {
        usage();
        return EXIT_SUCCESS;
    }

    if (argc == 5) {
        address = net::ip::make_address(argv[1]);
        port = static_cast<uint16_t>(std::atoi(argv[2]));
        tcp_address = argv[3];
        tcp_port = static_cast<uint16_t>(std::atoi(argv[4]));
    }

    std::thread(&tcp_sock_thread_func, tcp_address, tcp_port).detach();

    tcp::acceptor acceptor{ioc_get(), {address, port}};

    for (;;) {
        tcp::socket socket{ioc_get()};

        boost::system::error_code err;
        acceptor.accept(socket, err);
        if (err.value() != 0) {
            ERR("failed: acceptor.accept((): %d: %s", err.value(), err.message().c_str());
            return EXIT_FAILURE;
        }

        {
            std::lock_guard<std::mutex> lock(ws_list_mutex);
            ws_list.emplace_back(ws_t(std::move(socket)));
        }

        std::thread(&ws_thread_func, std::ref(ws_list.back()), client_id).detach();

        client_count++;
        client_id++;
        LOG("client_count: %d client_id: %d", client_count, client_id);
    }
}
