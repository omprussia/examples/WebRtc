#pragma once

#include <stdarg.h>
#include <stdio.h>

#define LOG(format, ...) \
    do { \
        printf("%s:%d:" format "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__); \
        fflush(stdout); \
    } while (0)

#define ERR(format, ...) \
    do { \
        fprintf(stderr, "%s:%d:" format "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__); \
        fflush(stderr); \
    } while (0)
