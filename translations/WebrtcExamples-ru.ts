<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="22"/>
        <source>About Application</source>
        <translation>О&#xa0;приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Приложение для показа работы WebRTC на ОС Аврора&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="41"/>
        <source>3-Clause BSD License</source>
        <translation>Лицензия 3-Clause&#xa0;BSD</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright&#xa0;(C)&#xa0;2022 ru.auroraos&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
	&lt;li&gt;Redistributions of&#xa0;source code must retain the above copyright notice, this list of&#xa0;conditions and the following disclaimer.&lt;/li&gt;
	&lt;li&gt;Redistributions in&#xa0;binary form must reproduce the above copyright notice, this list of&#xa0;conditions and the following disclaimer in&#xa0;the documentation and/or other materials provided with the distribution.&lt;/li&gt;
	&lt;li&gt;Neither the name of&#xa0;the copyright holder nor the names of&#xa0;its contributors may be&#xa0;used to&#xa0;endorse or&#xa0;promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS&#xa0;PROVIDED BY&#xa0;THE&#xa0;COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS&#xa0;IS&quot; AND ANY EXPRESS OR&#xa0;IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE&#xa0;IMPLIED WARRANTIES OF&#xa0;MERCHANTABILITY AND FITNESS FOR A&#xa0;PARTICULAR PURPOSE ARE DISCLAIMED. IN&#xa0;NO&#xa0;EVENT SHALL THE&#xa0;COPYRIGHT HOLDER OR&#xa0;CONTRIBUTORS BE&#xa0;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR&#xa0;CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF&#xa0;SUBSTITUTE GOODS OR&#xa0;SERVICES; LOSS OF&#xa0;USE, DATA, OR&#xa0;PROFITS; OR&#xa0;BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON&#xa0;ANY THEORY OF&#xa0;LIABILITY, WHETHER IN&#xa0;CONTRACT, STRICT LIABILITY, OR&#xa0;TORT (INCLUDING NEGLIGENCE OR&#xa0;OTHERWISE) ARISING IN&#xa0;ANY WAY OUT OF&#xa0;THE&#xa0;USE OF&#xa0;THIS SOFTWARE, EVEN IF&#xa0;ADVISED OF&#xa0;THE&#xa0;POSSIBILITY OF&#xa0;SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="11"/>
        <source>WebRTC Examples</source>
        <translation>WebRTC Примеры</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="77"/>
        <source>WebRTC Examples</source>
        <translation>WebRTC Примеры</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="107"/>
        <location filename="../qml/pages/MainPage.qml" line="311"/>
        <source>Connect</source>
        <translation>Подключение</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="118"/>
        <source>Call</source>
        <translation>Звонок</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="129"/>
        <source>Disable encryption</source>
        <translation>Отключить шифрование</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="140"/>
        <source>Echo cancellation</source>
        <translation>Подавление эха</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="151"/>
        <source>Auto volume control</source>
        <translation>Авторегулировка громкости</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="162"/>
        <source>Noise suppression</source>
        <translation>Шумоподавление</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="173"/>
        <source>Camera type:</source>
        <translation>Тип камеры:</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="176"/>
        <source>Front</source>
        <translation>Передняя</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="180"/>
        <source>Rear</source>
        <translation>Задняя</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="184"/>
        <source>Fake</source>
        <translation>Поддельная</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="197"/>
        <location filename="../qml/pages/MainPage.qml" line="198"/>
        <source>Server</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="213"/>
        <location filename="../qml/pages/MainPage.qml" line="214"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="233"/>
        <location filename="../qml/pages/MainPage.qml" line="234"/>
        <source>Camera resolution</source>
        <translation>Разрешение камеры</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="249"/>
        <location filename="../qml/pages/MainPage.qml" line="250"/>
        <source>Stun list</source>
        <translation>Stun список</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="264"/>
        <location filename="../qml/pages/MainPage.qml" line="265"/>
        <source>Force fieldtrials</source>
        <translation>Экспериментальные характеристики</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="60"/>
        <source>Select file for sending</source>
        <translation>Выбрать файл для отправки</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="279"/>
        <location filename="../qml/pages/MainPage.qml" line="280"/>
        <source>Message</source>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="295"/>
        <source>Sending file</source>
        <translation>Файл для отправки</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="325"/>
        <source>Choose file</source>
        <translation>Выбрать файл</translation>
    </message>
</context>
</TS>
