# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2024
* Ivan Matveev, <i.matveev@omp.ru>
  * Developer, 2024
* Evgeniy Samohin, <e.samohin@omp.ru>
  * Maintainer, 2024
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
* Oleg Shevchenko
  * Reviewer, 2024
* Andrey Vasilyev
  * Reviewer, 2024
