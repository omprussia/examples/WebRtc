// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include <libwebrtc/api/video/i420_buffer.h>
#include <libwebrtc/aurora/StreamCameraVideoBufferNative.h>
#include <libwebrtc/rtc_base/system/rtc_export.h>
#include <libwebrtc/rtc_base/checks.h>
#include <libwebrtc/rtc_base/ssl_adapter.h>

#include <fstream>
#include <tuple>

#include <QTimer>

#include "datachannelProtocol.h"
#include "StaticMethods.h"
#include "PeerConnectionContext.h"
#include "VideoTrackSourceHolder.h"

#include "VideoController.h"

VideoController::VideoController() { }

VideoController::~VideoController()
{
    rtc::CleanupSSL();
    m_videoTrackSourceLocal->RemoveSinks();
    m_peerConnectionObserver.reset();
    m_videoTrackSourceLocal.reset();
}

Aurora::StreamCamera::VideoOutputQt5 *VideoController::videoOutputRemote()
{
    return &m_videoOutputRemote;
}

Aurora::StreamCamera::VideoOutputQt5 *VideoController::videoOutputLocal()
{
    return &m_videoOutputLocal;
}

int VideoController::cameraMountAngle()
{
    return m_cameraMountAngle.load();
}

int VideoController::screenAngle()
{
    return m_screenAngle.load();
}

void VideoController::setScreenAngle(int screenAngle)
{
    m_screenAngle = screenAngle;
    Q_EMIT screenAngleChanged();
}

void connectVideoSinkToVideoOutput(VideoSinkCallback *video_sink,
                                   Aurora::StreamCamera::VideoOutputQt5 *videoOutput)
{
    video_sink->OnFrameCallback = [videoOutput](const webrtc::VideoFrame *videoFrame) {
        RTC_DCHECK(videoFrame);
        rtc::scoped_refptr<webrtc::VideoFrameBuffer> webrtc_buffer =
                videoFrame->video_frame_buffer();

        if (webrtc_buffer->type() == webrtc::VideoFrameBuffer::Type::kNative) {
            const StreamCameraVideoBufferNative *buffer =
                    reinterpret_cast<const StreamCameraVideoBufferNative *>(webrtc_buffer.get());
            RTC_DCHECK(buffer);
            if (buffer->graphicBuffer_)
                videoOutput->onGraphicBuffer(buffer->graphicBuffer_);
            else if (buffer->ycbcr_)
                videoOutput->onVideoFrame(buffer->ycbcr_.get());
            return;
        }

        if (webrtc_buffer->type() == webrtc::VideoFrameBuffer::Type::kI420) {
            const webrtc::I420Buffer *buffer =
                    static_cast<const webrtc::I420Buffer *>(webrtc_buffer.get());
            auto ycbcr = std::make_shared<Aurora::StreamCamera::YCbCrFrame>();
            ycbcr->chromaStep = 1;
            ycbcr->width = buffer->width();
            ycbcr->height = buffer->height();
            ycbcr->yStride = buffer->StrideY();
            ycbcr->cStride = buffer->StrideU();
            ycbcr->y = buffer->DataY();
            ycbcr->cb = buffer->DataU();
            ycbcr->cr = buffer->DataV();
            videoOutput->onVideoFrame(ycbcr.get());
            return;
        }
    };
}

std::tuple<bool, std::vector<uint8_t>> readFile(const std::string &file_name)
{
    std::ifstream ifs;
    ifs.open(file_name, std::ios::binary);
    if (!ifs.is_open()) {
        RTC_LOG(LS_ERROR) << "failed: ifs.open()"
                          << ": " << file_name << ": " << std::strerror(errno);
        return { false, {} };
    }
    std::istreambuf_iterator<char> start(ifs), end;
    std::vector<uint8_t> data(start, end);
    ifs.close();
    return { true, data };
}

bool writeFile(const std::string &file_name, const char *data, size_t size, bool append)
{
    const char *mode;
    if (append)
        mode = "a";
    else
        mode = "w";
    FILE *fh = fopen(file_name.c_str(), mode);
    if (fh == nullptr) {
        RTC_LOG(LS_ERROR) << "failed: fopen(): " << file_name.c_str() << ":"
                          << std::strerror(errno);
        return false;
    }
    ssize_t written = fwrite(data, 1, size, fh);
    if (written != static_cast<ssize_t>(size)) {
        RTC_LOG(LS_ERROR) << "failed: fwrite(): " << file_name.c_str() << ":"
                          << std::strerror(errno);
        fclose(fh);
        return false;
    }
    fclose(fh);
    return true;
}

bool writeFile(const std::string &file_name, const std::vector<uint8_t> &data, bool append)
{
    return writeFile(file_name, reinterpret_cast<const char *>(data.data()), data.size(), append);
}

DataChannelObserverImpl *
createDataChannelObserverImpl(PeerConnectionObserverImpl *pco,
                              rtc::scoped_refptr<webrtc::DataChannelInterface> channel)
{
    DataChannelObserverImpl *dco = new DataChannelObserverImpl();
    if (!channel) {
        webrtc::DataChannelInit config;
        // TODO CreateDataChannel() is deprecated: use CreateDataChannelOrError()
        channel = pco->peer_connection_->CreateDataChannel("data_channel", &config);
    }
    dco->data_channel_ = channel;
    dco->onmessage = [](const webrtc::DataBuffer &buffer) {
        namespace DCP = datachannelProtocol;
        auto json = std::string(buffer.data.data<char>(), buffer.data.size());
        RTC_LOG_F(LS_INFO) << ": " << json;
        DCP::parseMessage(
                json,
                [](const std::string &text_from_msg) {
                    std::string text = text_from_msg + "\n";
                    writeFile("dataChannelText.log", text.c_str(), text.size(), true);
                },
                [](DCP::file_chunk_info_t chunk_info) {
                    RTC_LOG_F(LS_INFO) << chunk_info.file_name << ":" << chunk_info.chunk_idx;
                    if (chunk_info.chunk_idx == 0)
                        writeFile(chunk_info.file_name, chunk_info.chunk, false);
                    else
                        writeFile(chunk_info.file_name, chunk_info.chunk, true);
                });
    };
    dco->data_channel_->RegisterObserver(dco);
    return dco;
}

void sendDataChannel(DataChannelObserverImpl *dco, const Config &config)
{
    if (config.send_text.size() > 0)
        datachannelProtocol::send_text(dco, config.send_text);
    if (config.send_file.size() == 0)
        return;
    RTC_LOG_F(LS_INFO) << "send_file_data():" << config.send_file;
    auto [ok, file_data] = readFile(config.send_file);
    if (!ok) {
        RTC_LOG_F(LS_WARNING) << "failed: read_file(): " << config.send_file;
        return;
    }
    datachannelProtocol::send_file_data(dco, config.send_file, file_data);
}

void VideoController::connect(int port, bool connect, bool call, bool disableEncryption,
                              bool echoCancellation, bool autoGainControl, bool noiseSuppression,
                              QString server, QString cameraType, QString cameraResolution,
                              QString stunList, QString forceFieldtrials, QString sendText,
                              QString sendFile)
{
    rtc::LogMessage::LogToDebug(rtc::LS_INFO);

    m_config = StaticMethods::createConfig(server.toStdString(), port, connect, call,
                                           disableEncryption, stunList.toStdString(),
                                           cameraType.toStdString(), cameraResolution.toStdString(),
                                           sendText.toStdString(), sendFile.toStdString());
    m_peerConnectionContext.echoCancellation = echoCancellation;
    m_peerConnectionContext.autoGainControl = autoGainControl;
    m_peerConnectionContext.noiseSuppression = noiseSuppression;
    rtc::InitializeSSL();

    m_videoTrackSourceLocal =
            std::make_unique<VideoTrackSourceHolder>(nullptr, m_peerConnectionContext, nullptr);
    m_screenAngle = m_screen->angleBetween(m_screen->orientation(), Qt::PrimaryOrientation);
    Q_EMIT screenAngleChanged();
    QObject::connect(m_screen, &QScreen::orientationChanged, this,
                     [this](Qt::ScreenOrientation orientation) {
                         m_screenAngle =
                                 m_screen->angleBetween(orientation, Qt::PrimaryOrientation);
                         RTC_LOG_F(LS_INFO) << "orientationChanged: " << orientation
                                            << " screenAngle: " << m_screenAngle;
                         Q_EMIT screenAngleChanged();
                     });
    m_videoTrackSourceLocal->video_track_source_ = StaticMethods::createVideoTrackSourceLocal(
            m_config.camera_type, m_config.camera_resolution, &m_cameraMountAngle, [this]() {
                int deg = m_screenAngle + m_cameraMountAngle;
                deg -= 360. * std::floor(deg * (1. / 360.));
                return deg;
            });
    Q_EMIT cameraMountAngleChanged();
    m_videoTrackSourceLocal->ConnectSink(m_videoSinkLocal);

    connectVideoSinkToVideoOutput(&m_videoSinkRemote, &m_videoOutputRemote);
    connectVideoSinkToVideoOutput(&m_videoSinkLocal, &m_videoOutputLocal);

    Q_EMIT videoOutputLocalChanged();
    Q_EMIT videoOutputRemoteChanged();

    QObject::connect(
            &m_websocket, &Websocket::textMessageReceived, this,
            [this](const std::string &message) {
                RTC_LOG_F(LS_INFO) << "onMessage: " << message;
                ProtocolResponse msg = protocolParseMessage(message);
                if (msg.error) {
                    RTC_LOG_F(LS_WARNING) << "failed: protocolParseMessage()";
                    return;
                }
                if (msg.offer) {
                    RTC_LOG_F(LS_INFO) << "Received offer";
                    m_peerConnectionObserver.reset(
                            m_peerConnectionContext.CreatePeerConnectionObserver(
                                    m_config, m_videoTrackSourceLocal->video_track_source_.get(),
                                    m_videoSinkRemote, m_websocket));
                    m_peerConnectionObserver->ondatachannel =
                            [this](rtc::scoped_refptr<webrtc::DataChannelInterface> channel) {
                                m_dataChannelObserver.reset(createDataChannelObserverImpl(
                                        m_peerConnectionObserver.get(), channel));
                            };
                    m_peerConnectionObserver->SetRemoteDescription(msg.sessionDescriptionInfo);
                    m_peerConnectionObserver->CreateAnswer();
                    return;
                }
                if (!m_peerConnectionObserver) {
                    RTC_LOG_F(LS_INFO) << "!pco";
                    return;
                }
                if (msg.answer) {
                    RTC_LOG_F(LS_INFO) << "Received answer";
                    m_peerConnectionObserver->SetRemoteDescription(msg.sessionDescriptionInfo);
                    return;
                }
                if (msg.disconnect) {
                    RTC_LOG_F(LS_INFO) << "Received disconnect";
                    m_peerConnectionObserver.reset(nullptr);
                    return;
                }
                if (msg.endOfCandidates) {
                    RTC_LOG_F(LS_INFO) << "Received end-of-candidates";
                    return;
                }
                if (msg.iceCandidate) {
                    RTC_LOG_F(LS_INFO) << "Received candidate";
                    m_peerConnectionObserver->AddIceCandidate(msg.iceCandidateInfo);
                    return;
                }
                RTC_LOG_F(LS_WARNING) << "Received unknown message: " << message;
            });

    bool connecting = false;
    QObject::connect(&m_websocket, &Websocket::connected, this, [this, &connecting]() {
        RTC_LOG_F(LS_INFO) << "connected: ";
        connecting = false;
        if (!m_config.call)
            return;
        m_peerConnectionObserver.reset(m_peerConnectionContext.CreatePeerConnectionObserver(
                m_config, m_videoTrackSourceLocal->video_track_source_.get(), m_videoSinkRemote,
                m_websocket));
        m_peerConnectionObserver->CreateOffer();
    });

    QObject::connect(&m_websocket, &Websocket::disconnected, this, [this, &connecting]() {
        RTC_LOG_F(LS_INFO) << "disconnected: ";
        connecting = false;
        m_peerConnectionObserver.reset(nullptr);
    });

    m_timer.setInterval(1000);
    QObject::connect(&m_timer, &QTimer::timeout, this, [this, server, port, &connecting]() {
        if (m_websocket.m_connected)
            return;
        if (connecting)
            return;
        connecting = true;
        auto url = StaticMethods::getServerUrl(server.toStdString(), port);
        if (m_config.connect)
            m_websocket.client(url);
        else
            m_websocket.server(url.port());
    });
    m_timer.start();

    bool send_flag = false;
    m_dataChannelTimer.setInterval(3000);
    QObject::connect(&m_timer, &QTimer::timeout, [this, &send_flag]() {
        if (!m_dataChannelObserver)
            return;
        if (send_flag)
            return;
        if (std::string("open") != m_dataChannelObserver->readyState())
            return;
        send_flag = true;
        sendDataChannel(m_dataChannelObserver.get(), m_config);
    });
    m_dataChannelTimer.start();
}
