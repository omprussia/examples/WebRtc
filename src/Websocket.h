// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include <QObject>
#include <QString>
#include <QVariant>
#include <QWebSocket>
#include <QWebSocketCorsAuthenticator>
#include <QWebSocketServer>

#include <atomic>
#include <functional>
#include <memory>

class Websocket : public QObject
{
    Q_OBJECT
public:
    Websocket(QObject *parent = nullptr);
    ~Websocket() override = default;
    bool server(uint16_t port);
    void client(const QUrl &url);
    void close();
    void sendMessage(QString message);
    void wsReset(QWebSocket *ws);
    void wsServerReset(QWebSocketServer *ws_server);
    bool isServer();

signals:
    void connected();
    void disconnected();
    void textMessageReceived(std::string message);
public slots:
    void connectServer(const QString &url, bool connect);
    void sendMessageSlot(QString message);

private:
    void disconnected_on_error(const char *log_message);
    void disconnected_emit(const char *log_message);

public:
    QWebSocket *m_ws = nullptr;
    QWebSocketServer *m_ws_server = nullptr;
    std::atomic<bool> m_connected = ATOMIC_VAR_INIT(false);
    uint16_t m_port = 0;
    QUrl m_url;
};

#endif // WEBSOCKET_H
