// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef PEERCONNECTIONCONTEXT_H
#define PEERCONNECTIONCONTEXT_H

#include <libwebrtc/pc/video_track_source.h>
#include <libwebrtc/api/scoped_refptr.h>
#include <libwebrtc/api/peer_connection_interface.h>
#include <libwebrtc/api/create_peerconnection_factory.h>
#include <libwebrtc/api/scoped_refptr.h>
#include <libwebrtc/rtc_base/thread.h>

#include "Config.h"
#include "PeerConnectionObserverImpl.h"
#include "Websocket.h"
#include "VideoSink.h"
#include "video/VideoTrackSourceImpl.h"

class PeerConnectionContext
{
public:
    PeerConnectionContext();
    ~PeerConnectionContext();

    rtc::scoped_refptr<webrtc::AudioTrackInterface> CreateAudioTrack();
    rtc::scoped_refptr<webrtc::VideoTrackInterface>
    CreateVideoTrack(webrtc::VideoTrackSourceInterface *source);

    void CreatePeerConnectionFactory();
    PeerConnectionObserverImpl *
    CreatePeerConnectionObserver(const Config &appConfig,
                                 webrtc::VideoTrackSource *video_track_source_local,
                                 VideoSinkCallback &video_sink_remote, Websocket &websocket);

    rtc::Thread *signaling_thread() { return signaling_thread_.get(); }
    rtc::Thread *network_thread()
    {
        return network_thread_.get();
        ;
    }
    rtc::Thread *worker_thread() { return worker_thread_.get(); }

    std::unique_ptr<rtc::Thread> signaling_thread_ = nullptr;
    std::unique_ptr<rtc::Thread> network_thread_ = nullptr;
    std::unique_ptr<rtc::Thread> worker_thread_ = nullptr;

    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> peer_connection_factory_ = nullptr;
    webrtc::PeerConnectionInterface::RTCOfferAnswerOptions offer_answer_options_ = {};
    std::string video_encoder_name_ = "auto";
    std::string video_decoder_name_ = "auto";

    bool echoCancellation;
    bool autoGainControl;
    bool noiseSuppression;
};

#endif // PEERCONNECTIONCONTEXT_H
