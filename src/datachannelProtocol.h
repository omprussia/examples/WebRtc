// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef DATACHANNELPROTOCOL_H
#define DATACHANNELPROTOCOL_H

#include "DataChannelObserverImpl.h"

#include "rtc_base/strings/json.h"
#include "rtc_base/third_party/base64/base64.h"

#include <functional>
#include <string>
#include <vector>

namespace datachannelProtocol {

using chunk_t = std::vector<uint8_t>;

struct file_chunk_info_t
{
    std::string file_name;
    size_t chunk_idx;
    size_t chunk_count;
    chunk_t chunk;
};

bool send_text(DataChannelObserverImpl *dco, const std::string &text);
bool send_file_data(DataChannelObserverImpl *dco, const std::string &file_name,
                    const std::vector<uint8_t> &data);

bool parseMessage(const std::string &message, std::function<void(std::string)> text_cb,
                  std::function<void(file_chunk_info_t)> file_chunk_cb);

} // namespace datachannelProtocol

#endif // DATACHANNELPROTOCOL_H
