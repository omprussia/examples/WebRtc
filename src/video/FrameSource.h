// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef FRAMESOURCE_H
#define FRAMESOURCE_H

#include <libwebrtc/api/video/video_frame.h>

class FrameSource
{
public:
    virtual ~FrameSource() = default;
    virtual webrtc::VideoFrame GetFrame() = 0;
};

#endif // FRAMESOURCE_H
