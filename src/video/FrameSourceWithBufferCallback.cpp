// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "FrameSourceWithBufferCallback.h"
#include "../Clock.h"

#include <libwebrtc/api/scoped_refptr.h>
#include <libwebrtc/api/video/i420_buffer.h>
#include <libwebrtc/api/video/video_frame_buffer.h>
#include <libwebrtc/rtc_base/checks.h>
#include <libwebrtc/rtc_base/logging.h>

#include <mutex>

FrameSourceWithBufferCallback::FrameSourceWithBufferCallback(int width, int height)
    : width_(width), height_(height)
{
    RTC_LOG_F(LS_INFO) << width_ << "x" << height_;
    RTC_CHECK_GT(width_, 0);
    RTC_CHECK_GT(height_, 0);
}

webrtc::VideoFrame FrameSourceWithBufferCallback::GetFrame()
{
    return GetFrame(width_, height_);
}

FrameSourceWithBufferCallback::~FrameSourceWithBufferCallback()
{
    RTC_LOG_F(LS_INFO);
}

webrtc::VideoFrame FrameSourceWithBufferCallback::GetFrame(int width, int height)
{
    std::scoped_lock lock(mutex_);
    RTC_CHECK_GT(width, 0);
    RTC_CHECK_GT(height, 0);

    rtc::scoped_refptr<webrtc::I420Buffer> buffer = webrtc::I420Buffer::Create(width, height);
    RTC_DCHECK(buffer);
    on_frame_callback_(*buffer);

    webrtc::VideoFrame frame = webrtc::VideoFrame::Builder()
                                       .set_video_frame_buffer(buffer)
                                       .set_timestamp_us(uptime_us())
                                       .build();
    return frame;
}
