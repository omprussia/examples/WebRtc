// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef VIDEOTRACKSOURCEIMPL_H
#define VIDEOTRACKSOURCEIMPL_H

#include <libwebrtc/api/video/video_source_interface.h>
#include <libwebrtc/pc/video_track_source.h>

#include <memory>

class VideoTrackSourceImpl : public webrtc::VideoTrackSource
{
public:
    VideoTrackSourceImpl(std::shared_ptr<rtc::VideoSourceInterface<webrtc::VideoFrame>> source,
                         bool remote)
        : VideoTrackSource(remote), source_(source)
    {
        RTC_LOG_F(LS_INFO);
    }

    ~VideoTrackSourceImpl() override { RTC_LOG_F(LS_INFO); }

    rtc::VideoSourceInterface<webrtc::VideoFrame> *source() override
    {
        RTC_LOG_F(LS_INFO);
        return source_.get();
    }

    std::shared_ptr<rtc::VideoSourceInterface<webrtc::VideoFrame>> source_;
};

#endif // VIDEOTRACKSOURCEIMPL_H
