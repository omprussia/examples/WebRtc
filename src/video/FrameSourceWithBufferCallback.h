// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef FRAMESOURCEWITHBUFFERCALLBACK_H
#define FRAMESOURCEWITHBUFFERCALLBACK_H

#include "FrameSource.h"

#include <libwebrtc/api/video/i420_buffer.h>
#include <libwebrtc/api/video/video_frame.h>

#include <functional>
#include <mutex>

class FrameSourceWithBufferCallback final : public FrameSource
{
public:
    FrameSourceWithBufferCallback(int width, int height);
    ~FrameSourceWithBufferCallback() override;
    webrtc::VideoFrame GetFrame() override;

    webrtc::VideoFrame GetFrame(int width, int height);

    const int width_;
    const int height_;
    std::function<void(webrtc::I420Buffer &)> on_frame_callback_ = [](webrtc::I420Buffer &buffer) {
    };
    // Lock this mutex to change on_frame_callback_
    std::mutex mutex_;
};

#endif // FRAMESOURCEWITHBUFFERCALLBACK_H
