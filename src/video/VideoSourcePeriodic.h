// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef VIDEOSOURCEPERIODIC_H
#define VIDEOSOURCEPERIODIC_H

#include <memory>

#include "FrameSource.h"

#include <libwebrtc/api/task_queue/default_task_queue_factory.h>
#include <libwebrtc/api/video/video_source_interface.h>
#include <libwebrtc/media/base/video_broadcaster.h>
#include <libwebrtc/rtc_base/logging.h>
#include <libwebrtc/rtc_base/synchronization/mutex.h>
#include <libwebrtc/rtc_base/task_queue.h>
#include <libwebrtc/rtc_base/task_utils/repeating_task.h>

using namespace webrtc;

class VideoSourcePeriodic final : public rtc::VideoSourceInterface<VideoFrame>
{
public:
    explicit VideoSourcePeriodic(std::shared_ptr<FrameSource> frame_source, int fps)
        : frame_source_(frame_source),
          fps_(fps),
          task_queue_factory_(CreateDefaultTaskQueueFactory()),
          task_queue_(std::make_unique<rtc::TaskQueue>(task_queue_factory_->CreateTaskQueue(
                  __FUNCTION__, TaskQueueFactory::Priority::NORMAL)))
    {
        RTC_LOG_F(LS_INFO);
        MutexLock lock(&mutex_);
        TimeDelta frame_interval = TimeDelta::Millis(1000 / fps);
        RepeatingTaskHandle::Start(task_queue_->Get(), [this, frame_interval] {
            broadcaster_.OnFrame(frame_source_->GetFrame());
            return frame_interval;
        });
    }

    ~VideoSourcePeriodic() override
    {
        RTC_LOG_F(LS_INFO);
        Stop();
    }

    rtc::VideoSinkWants wants() const
    {
        RTC_LOG_F(LS_INFO);
        MutexLock lock(&mutex_);
        return wants_;
    }

    void RemoveSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink) override
    {
        RTC_LOG_F(LS_INFO) << " sink: " << static_cast<void *>(sink);
        broadcaster_.RemoveSink(sink);
    }

    void AddOrUpdateSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink,
                         const rtc::VideoSinkWants &wants) override
    {
        RTC_LOG_F(LS_INFO) << " sink: " << static_cast<void *>(sink)
                           << " max_pixel_count: " << wants.max_pixel_count
                           << " target_pixel_count: " << wants.target_pixel_count.value_or(0)
                           << " max_framerate_fps: " << wants.max_framerate_fps
                           << " resolution_alignment: " << wants.resolution_alignment
                           << " resolutions count: " << wants.resolutions.size();
        for (auto resolution : wants.resolutions)
            RTC_LOG(LS_INFO) << resolution.width << "x" << resolution.height;

        {
            MutexLock lock(&mutex_);
            wants_ = wants;
        }
        broadcaster_.AddOrUpdateSink(sink, wants);
    }

    void Stop()
    {
        RTC_LOG_F(LS_INFO);
        RTC_DCHECK(task_queue_);
        task_queue_.reset();
    }

    rtc::VideoBroadcaster broadcaster_;
    std::shared_ptr<FrameSource> frame_source_;
    int fps_;
    mutable Mutex mutex_;
    rtc::VideoSinkWants wants_ RTC_GUARDED_BY(&mutex_);
    std::unique_ptr<TaskQueueFactory> task_queue_factory_;
    std::unique_ptr<rtc::TaskQueue> task_queue_;
};

#endif // VIDEOSOURCEPERIODIC_H
