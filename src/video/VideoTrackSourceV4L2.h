// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef VIDEOTRACKSOURCEV4L2_H
#define VIDEOTRACKSOURCEV4L2_H

#include <libwebrtc/modules/video_capture/video_capture.h>
#include <libwebrtc/modules/video_capture/video_capture_factory.h>
#include <libwebrtc/pc/video_track_source.h>
#include <libwebrtc/rtc_base/logging.h>
#include <libwebrtc/test/vcm_capturer.h>
#include <libwebrtc/third_party/abseil-cpp/absl/memory/memory.h>

class VideoTrackSourceV4L2 : public webrtc::VideoTrackSource
{
public:
    static rtc::scoped_refptr<VideoTrackSourceV4L2> Create(size_t width, size_t height)
    {
        const size_t kFps = 30;
        std::unique_ptr<webrtc::test::VcmCapturer> capturer;
        std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> info(
                webrtc::VideoCaptureFactory::CreateDeviceInfo());
        if (!info) {
            return 0;
        }
        int num_devices = info->NumberOfDevices();
        for (int i = 0; i < num_devices; ++i) {
            capturer = absl::WrapUnique(webrtc::test::VcmCapturer::Create(width, height, kFps, i));
            if (capturer) {
                return rtc::scoped_refptr<VideoTrackSourceV4L2>(
                        new rtc::RefCountedObject<VideoTrackSourceV4L2>(std::move(capturer)));
            }
        }

        return nullptr;
    }

    ~VideoTrackSourceV4L2() { RTC_LOG_F(LS_INFO); }

protected:
    explicit VideoTrackSourceV4L2(std::unique_ptr<webrtc::test::VcmCapturer> capturer)
        : VideoTrackSource(/*remote=*/false), capturer_(std::move(capturer))
    {
        RTC_LOG_F(LS_INFO);
    }

private:
    rtc::VideoSourceInterface<webrtc::VideoFrame> *source() override { return capturer_.get(); }

    std::unique_ptr<webrtc::test::VcmCapturer> capturer_;
};

#endif // VIDEOTRACKSOURCEV4L2_H
