// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include <libwebrtc/pc/video_track.h>
#include <libwebrtc/pc/media_stream_track_proxy.h>
#include <libwebrtc/api/audio_codecs/builtin_audio_decoder_factory.h>
#include <libwebrtc/api/audio_codecs/builtin_audio_encoder_factory.h>
#include <libwebrtc/api/video_codecs/builtin_video_decoder_factory.h>
#include <libwebrtc/api/video_codecs/builtin_video_encoder_factory.h>
#include <libwebrtc/api/video_codecs/video_decoder_factory.h>
#include <libwebrtc/api/video_codecs/video_encoder_factory.h>

#include "PeerConnectionContext.h"

PeerConnectionContext::PeerConnectionContext()
{
    network_thread_ = rtc::Thread::CreateWithSocketServer();
    network_thread_->SetName("network_thread_", nullptr);
    network_thread_->Start();

    signaling_thread_ = rtc::Thread::CreateWithSocketServer();
    signaling_thread_->SetName("signaling_thread_", nullptr);
    signaling_thread_->Start();
    RTC_DCHECK(!signaling_thread_->IsCurrent());

    worker_thread_ = rtc::Thread::Create();
    worker_thread_->SetName("worker_thread_", nullptr);
    worker_thread_->Start();
}

PeerConnectionContext::~PeerConnectionContext() { }

cricket::AudioOptions CreateAudioOptions(bool echoCancellation, bool autoGainControl,
                                         bool noiseSuppression)
{
    cricket::AudioOptions audio_options{};
    // OMP#OS-14169
    // Options disabled to workaround 1-2s audio delay
    // from phone to browser.
    audio_options.highpass_filter = false;

    audio_options.echo_cancellation = echoCancellation;
    audio_options.auto_gain_control = autoGainControl;
    audio_options.noise_suppression = noiseSuppression;

    return audio_options;
}

static webrtc::PeerConnectionInterface::RTCOfferAnswerOptions MakeOfferAnswerOptions()
{
    webrtc::PeerConnectionInterface::RTCOfferAnswerOptions options;
    // Disable bundle rtp to workaround:
    // peerconnection_client_wait_direct.sh.log:[052:603](rtp_to_ntp_estimator.cc:149):
    // Newer RTCP SR report with older RTP timestamp, dropping
    options.use_rtp_mux = false;
    options.voice_activity_detection = false;
    return options;
}

rtc::scoped_refptr<webrtc::AudioTrackInterface> PeerConnectionContext::CreateAudioTrack()
{
    RTC_DCHECK(signaling_thread_->IsCurrent());
    auto audio_source = peer_connection_factory_->CreateAudioSource(
            CreateAudioOptions(echoCancellation, autoGainControl, noiseSuppression));
    rtc::scoped_refptr<webrtc::AudioTrackInterface> track(
            peer_connection_factory_->CreateAudioTrack(kAudioLabel, audio_source.get()));
    return track;
}

rtc::scoped_refptr<webrtc::VideoTrackInterface>
PeerConnectionContext::CreateVideoTrack(webrtc::VideoTrackSourceInterface *source)
{
    RTC_DCHECK(signaling_thread_->IsCurrent());

    rtc::scoped_refptr<webrtc::VideoTrackInterface> track(webrtc::VideoTrack::Create(
            kVideoLabel, rtc::scoped_refptr<webrtc::VideoTrackSourceInterface>(source),
            worker_thread_.get()));
    return webrtc::VideoTrackProxy::Create(signaling_thread_.get(), worker_thread_.get(), track);
}

void PeerConnectionContext::CreatePeerConnectionFactory()
{
    peer_connection_factory_ = webrtc::CreatePeerConnectionFactory(
            network_thread_.get(), worker_thread_.get(), signaling_thread_.get(),
            nullptr, // AudioDeviceModule
            webrtc::CreateBuiltinAudioEncoderFactory(), webrtc::CreateBuiltinAudioDecoderFactory(),
            webrtc::CreateBuiltinVideoEncoderFactory(), webrtc::CreateBuiltinVideoDecoderFactory(),
            nullptr /* audio_mixer */, nullptr /* audio_processing */);

    RTC_DCHECK(peer_connection_factory_);
}

PeerConnectionObserverImpl *PeerConnectionContext::CreatePeerConnectionObserver(
        const Config &appConfig, webrtc::VideoTrackSource *video_track_source_local,
        VideoSinkCallback &video_sink_remote, Websocket &websocket)
{
    CreatePeerConnectionFactory();

    auto pco = new PeerConnectionObserverImpl(signaling_thread(), &video_sink_remote);

    pco->offer_answer_options_ = MakeOfferAnswerOptions();

    webrtc::PeerConnectionInterface::RTCConfiguration config;
    config.sdp_semantics = webrtc::SdpSemantics::kUnifiedPlan;
    config.disable_ipv6 = true;
    config.continual_gathering_policy =
            webrtc::PeerConnectionInterface::ContinualGatheringPolicy::GATHER_CONTINUALLY;

    for (auto server : appConfig.stunTurnServerList) {
        webrtc::PeerConnectionInterface::IceServer iceServer;
        iceServer.uri = server.uri;
        iceServer.username = server.username;
        iceServer.password = server.password;
        config.servers.push_back(iceServer);
    }
    for (const auto &server : config.servers)
        RTC_LOG_F(LS_INFO) << server.uri << ":" << server.username << ":" << server.password;
    signaling_thread()->Invoke<void>(
            RTC_FROM_HERE, [this, &pco, &appConfig, video_track_source_local, &config]() {
                webrtc::PeerConnectionFactoryInterface::Options options{};
                if (appConfig.disableEncryption) {
                    options.disable_encryption = true;
                    peer_connection_factory_->SetOptions(options);
                }

                webrtc::PeerConnectionDependencies pc_dependencies(pco);
                auto result = peer_connection_factory_->CreatePeerConnectionOrError(
                        config, std::move(pc_dependencies));
                RTC_DCHECK(result.ok());
                pco->peer_connection_ = result.value();

                auto video_track = CreateVideoTrack(video_track_source_local);
                pco->peer_connection_->AddTrack(video_track, { kStreamId });
                auto audio_track = CreateAudioTrack();
                pco->peer_connection_->AddTrack(audio_track, { kStreamId });

                if (appConfig.disableEncryption) {
                    options.disable_encryption = false;
                    peer_connection_factory_->SetOptions(options);
                }
            });

    RTC_DCHECK(pco->peer_connection_);
    pco->message_sender_ = [&websocket](const std::string &message) {
        websocket.sendMessage(message.c_str());
    };

    pco->state_change_cb_ = [this](webrtc::PeerConnectionInterface::PeerConnectionState state) {
        RTC_LOG_F(LS_INFO);
    };
    return pco;
}
