// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef DATACHANNELOBSERVERIMPL_H
#define DATACHANNELOBSERVERIMPL_H

#include "api/peer_connection_interface.h"
#include "rtc_base/copy_on_write_buffer.h"

#include <functional>
#include <string>

class DataChannelObserverImpl : public webrtc::DataChannelObserver
{

public:
    void OnStateChange() override
    {
        webrtc::DataChannelInterface::DataState state = data_channel_->state();
        RTC_LOG_F(LS_INFO) << "state: " << data_channel_->DataStateString(data_channel_->state());
        switch (state) {
        case webrtc::DataChannelInterface::kOpen:
            onopen();
            break;
        case webrtc::DataChannelInterface::kClosed:
            onclose();
            break;
        case webrtc::DataChannelInterface::kConnecting:
        case webrtc::DataChannelInterface::kClosing:
        default:
            break;
        }
    }

    void OnMessage(const webrtc::DataBuffer &buffer) override
    {
        RTC_LOG_F(LS_INFO) << ": " << std::string(buffer.data.data<char>(), buffer.data.size());
        onmessage(buffer);
    }

    void OnBufferedAmountChange(uint64_t previous_amount) override
    {
        RTC_LOG_F(LS_INFO) << previous_amount;
    }

    const char *readyState()
    {
        if (!data_channel_)
            return nullptr;
        return data_channel_->DataStateString(data_channel_->state());
    }

    void close() { data_channel_->Close(); }

    bool send(const std::string &str) { return send(str.c_str(), str.size()); }

    bool send(const char *data, size_t size)
    {
        return send(reinterpret_cast<const uint8_t *>(data), size, false);
    }

    bool send(const uint8_t *data, size_t size, bool binary)
    {
        if (!data_channel_) {
            RTC_LOG_F(LS_WARNING) << "failed: !data_channel_";
            return false;
        }
        if (data_channel_->state() != webrtc::DataChannelInterface::kOpen) {
            RTC_LOG_F(LS_WARNING) << "failed: != webrtc::DataChannelInterface::kOpen";
            return false;
        }

        webrtc::DataBuffer buffer(rtc::CopyOnWriteBuffer(data, size), binary);
        return data_channel_->Send(buffer);
    }

    rtc::scoped_refptr<webrtc::DataChannelInterface> data_channel_ = nullptr;

    std::function<void()> onopen = []() {};
    std::function<void()> onclose = []() {};
    std::function<void(const webrtc::DataBuffer &buffer)> onmessage =
            [](const webrtc::DataBuffer &buffer) {};
};

#endif // DATACHANNELOBSERVERIMPL_H
