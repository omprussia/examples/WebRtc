// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef VIDEOTRACKSOURCEHOLDER_H
#define VIDEOTRACKSOURCEHOLDER_H

#include <libwebrtc/pc/video_track_source.h>

#include <QObject>

#include "PeerConnectionContext.h"
#include "VideoSink.h"

class VideoTrackSourceHolder : public QObject
{
    Q_OBJECT
public:
    VideoTrackSourceHolder(QObject *parent, PeerConnectionContext &pc_ctx,
                           rtc::scoped_refptr<webrtc::VideoTrackSource> video_track_source)
        : QObject(parent), pc_ctx_(pc_ctx), video_track_source_(video_track_source)
    {
    }

    ~VideoTrackSourceHolder() override
    {
        RTC_LOG_F(LS_INFO);
        RemoveSinks();
    };

    void ConnectSink(VideoSinkCallback &sink)
    {
        RTC_LOG_F(LS_INFO);
        auto sink_p = static_cast<rtc::VideoSinkInterface<webrtc::VideoFrame> *>(&sink);
        if (!video_track_source_) {
            RTC_LOG_F(LS_WARNING) << "!video_track_source_";
            return;
        }

        pc_ctx_.worker_thread_->Invoke<void>(RTC_FROM_HERE, [this, sink_p]() {
            rtc::VideoSinkWants wants{};
            video_track_source_->AddOrUpdateSink(sink_p, wants);
        });
        sink_list_.push_back(&sink);
    }

    void RemoveSinks()
    {
        RTC_LOG_F(LS_INFO);
        pc_ctx_.worker_thread_->Invoke<void>(RTC_FROM_HERE, [this]() {
            for (auto sink : sink_list_)
                video_track_source_->RemoveSink(sink);
        });
        sink_list_.clear();
    }

    webrtc::VideoTrackSource *get() const { return video_track_source_.get(); }

    PeerConnectionContext &pc_ctx_;
    rtc::scoped_refptr<webrtc::VideoTrackSource> video_track_source_;
    std::vector<VideoSinkCallback *> sink_list_;
};

#endif // VIDEOTRACKSOURCEHOLDER_H
