// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef CONFIG_H
#define CONFIG_H

#include <QUrl>

#include <string>
#include <vector>

struct StunTurnServer
{
    std::string uri;
    std::string username;
    std::string password;
};

struct Config
{
    QUrl serverUrl;
    bool connect;
    bool call;
    bool disableEncryption;
    std::vector<StunTurnServer> stunTurnServerList;
    std::string camera_type;
    std::string camera_resolution;
    std::string send_text;
    std::string send_file;
};

#endif // CONFIG_H
