// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "PeerConnectionObserverImpl.h"

#include "libwebrtc/third_party/abseil-cpp/absl/memory/memory.h"
#include "libwebrtc/third_party/abseil-cpp/absl/types/optional.h"
#include "libwebrtc/api/audio_options.h"
#include "libwebrtc/api/create_peerconnection_factory.h"
#include "libwebrtc/api/media_stream_interface.h"
#include "libwebrtc/rtc_base/checks.h"
#include "libwebrtc/rtc_base/logging.h"
#include "libwebrtc/rtc_base/ref_counted_object.h"
#include "libwebrtc/rtc_base/rtc_certificate_generator.h"

PeerConnectionObserverImpl::PeerConnectionObserverImpl(
        rtc::Thread *signaling_thread,
        rtc::VideoSinkInterface<webrtc::VideoFrame> *video_sink_remote)
    : signaling_thread_(signaling_thread), video_sink_remote_(video_sink_remote)
{
    RTC_LOG_F(LS_INFO);

    signaling_thread_->Invoke<void>(RTC_FROM_HERE, [this]() {
        signaling_thread_safety_.reset(new webrtc::ScopedTaskSafety());
    });
}

PeerConnectionObserverImpl::~PeerConnectionObserverImpl()
{
    RTC_LOG_F(LS_INFO);
    Disconnect();
    Close();
    signaling_thread_->Invoke<void>(RTC_FROM_HERE,
                                    [this]() { signaling_thread_safety_.reset(nullptr); });
    RTC_LOG_F(LS_INFO) << "finished";
}

//
// PeerConnectionObserver implementation.
//

void PeerConnectionObserverImpl::OnSignalingChange(
        webrtc::PeerConnectionInterface::SignalingState new_state)
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
    if (!peer_connection_->remote_description())
        return;
    RTC_LOG_F(LS_INFO);
    for (const auto &candidate : candidate_list_remote_)
        AddIceCandidate(candidate);
    candidate_list_remote_.clear();
}

void PeerConnectionObserverImpl::OnAddTrack(
        rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver,
        const std::vector<rtc::scoped_refptr<webrtc::MediaStreamInterface>> &streams)
{
    RTC_LOG_F(LS_INFO) << __FUNCTION__ << " " << receiver->id();

    if (receiver->track()->kind() != webrtc::MediaStreamTrackInterface::kVideoKind)
        return;
    auto *video_track = static_cast<webrtc::VideoTrackInterface *>(receiver->track().get());
    rtc::VideoSinkWants wants{};
    video_track->AddOrUpdateSink(video_sink_remote_, wants);
}

void PeerConnectionObserverImpl::OnRemoveTrack(
        rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver)
{
    RTC_LOG_F(LS_INFO) << __FUNCTION__ << " " << receiver->id();
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
}

void PeerConnectionObserverImpl::OnDataChannel(
        rtc::scoped_refptr<webrtc::DataChannelInterface> channel)
{
    RTC_LOG_F(LS_INFO);
    ondatachannel(channel);
}

void PeerConnectionObserverImpl::OnRenegotiationNeeded()
{
    RTC_LOG_F(LS_INFO);
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
}

void PeerConnectionObserverImpl::OnIceConnectionChange(
        webrtc::PeerConnectionInterface::IceConnectionState new_state)
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
}

void PeerConnectionObserverImpl::OnIceGatheringChange(
        webrtc::PeerConnectionInterface::IceGatheringState new_state)
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
}

void PeerConnectionObserverImpl::OnIceCandidate(const webrtc::IceCandidateInterface *candidate)
{
    RTC_LOG_F(LS_INFO) << " " << candidate->sdp_mline_index() << " "
                       << candidate->candidate().ToString();
    SendMessage(protocolIceCandidateMessage(candidate));
}

void PeerConnectionObserverImpl::OnIceConnectionReceivingChange(bool receiving)
{
    RTC_LOG_F(LS_INFO);
}

void PeerConnectionObserverImpl::OnIceCandidateError(const std::string &address, int port,
                                                     const std::string &url, int error_code,
                                                     const std::string &error_text)
{
    RTC_LOG_F(LS_INFO) << ": " << address << ":" << port << " " << url << " " << error_text;
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
}

void PeerConnectionObserverImpl::OnConnectionChange(
        webrtc::PeerConnectionInterface::PeerConnectionState new_state)
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
    if (peer_connection_->ice_connection_state()
        == webrtc::PeerConnectionInterface::kIceConnectionFailed)
        Disconnect();
    state_change_cb_(new_state);
}

void PeerConnectionObserverImpl::OnStandardizedIceConnectionChange(
        webrtc::PeerConnectionInterface::IceConnectionState new_state)
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
}

//
// PeerConnectionObserver implementation end.
//

void PeerConnectionObserverImpl::SetRemoteDescription(SessionDescriptionInfo desc)
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
    if (disconnecting_) {
        RTC_LOG_F(LS_INFO) << "disconnecting_";
        return;
    }

    auto srdo_cb = SetRemoteDescriptionObserverCb::Create([this](webrtc::RTCError error) {
        if (error.ok()) {
            RTC_LOG_F(LS_INFO) << "OK: SetRemoteDescription()";
            return;
        }
        RTC_LOG_F(LS_ERROR) << "failed: SetRemoteDescription(): " << ToString(error.type()) << ": "
                            << error.message();
        Disconnect();
    });

    signaling_thread_->Invoke<void>(RTC_FROM_HERE, [this, srdo_cb, desc]() {
        auto sdp = desc.toSessionDescription();
        RTC_DCHECK(sdp);
        peer_connection_->SetRemoteDescription(std::move(sdp), srdo_cb);
    });
}

void PeerConnectionObserverImpl::AddIceCandidate(const IceCandidateInfo &candidate)
{
    auto iceCandidate = candidate.toIceCandidate();
    RTC_LOG_F(LS_INFO) << iceCandidate->candidate().ToString();
    if (disconnecting_) {
        RTC_LOG_F(LS_WARNING) << "disconnecting_";
        return;
    }

    if (!peer_connection_->local_description()) {
        RTC_LOG_F(LS_INFO) << "!peer_connection_->local_description(): store candidate";
        candidate_list_remote_.push_back(candidate);
        return;
    }

    if (!peer_connection_->remote_description()) {
        RTC_LOG_F(LS_INFO) << "!peer_connection_->remote_description(): store candidate";
        candidate_list_remote_.push_back(candidate);
        return;
    }

    int delay_ms = 10;
    const IceCandidateInfo candidate_copy = candidate;
    signaling_thread_->PostDelayedTask(
            ToQueuedTask(signaling_thread_safety_->flag(),
                         [this, candidate = candidate_copy]() {
                             if (disconnecting_) {
                                 RTC_LOG_F(LS_WARNING) << "disconnecting_";
                                 return;
                             }
                             auto iceCandidate = candidate.toIceCandidate();
                             peer_connection_->AddIceCandidate(
                                     std::move(iceCandidate), [candidate](webrtc::RTCError error) {
                                         auto iceCandidate = candidate.toIceCandidate();
                                         if (!error.ok())
                                             RTC_LOG_F(LS_WARNING)
                                                     << "failed: AddIceCandidate()"
                                                     << iceCandidate->candidate().ToString();
                                     });
                         }),
            delay_ms);
}

void PeerConnectionObserverImpl::SendMessage(const std::string &message)
{
    RTC_LOG_F(LS_INFO) << message;
    if (disconnecting_)
        return;
    message_sender_(message);
}

void PeerConnectionObserverImpl::CreateOffer()
{
    RTC_LOG_F(LS_INFO);
    if (disconnecting_) {
        RTC_LOG_F(LS_WARNING) << "disconnecting_";
        return;
    }

    auto sldo_cb = SetLocalDescriptionObserverCb::Create([this](webrtc::RTCError error) {
        if (error.ok()) {
            RTC_LOG_F(LS_INFO) << "OK: SetLocalDescription()";
            auto desc = peer_connection_->local_description();
            SendMessage(protocolSdpMessage(desc));
            return;
        }
        RTC_LOG_F(LS_ERROR) << "failed: SetLocalDescription(): " << ToString(error.type()) << ": "
                            << error.message();
        Disconnect();
    });

    auto csdo_cb = CreateSessionDescriptionObserverCb::Create(
            [this, sldo_cb](webrtc::SessionDescriptionInterface *desc) {
                peer_connection_->SetLocalDescription(sldo_cb);
            },
            [this](webrtc::RTCError error) {
                RTC_LOG_F(LS_ERROR) << "failed: CreateOffer(): " << ToString(error.type()) << ": "
                                    << error.message();
                Disconnect();
            });

    signaling_thread_->Invoke<void>(RTC_FROM_HERE, [this, csdo_cb]() {
        peer_connection_->CreateOffer(csdo_cb.get(), offer_answer_options_);
    });
}

void PeerConnectionObserverImpl::CreateAnswer()
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);

    if (disconnecting_) {
        RTC_LOG_F(LS_WARNING) << "disconnecting_";
        return;
    }

    signaling_thread_->Invoke<void>(RTC_FROM_HERE, [this]() { CreateAnswerDo(); });
}

bool PeerConnectionObserverImpl::CreateAnswerDo()
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
    if (disconnecting_) {
        RTC_LOG_F(LS_WARNING) << "disconnecting_";
        return true;
    }

    // PeerConnection cannot create an answer in a state other than have-remote-offer or
    // have-local-pranswer
    auto signaling_state = peer_connection_->signaling_state();
    if (signaling_state != webrtc::PeerConnectionInterface::kHaveRemoteOffer
        && signaling_state != webrtc::PeerConnectionInterface::kHaveLocalPrAnswer)
        return false;

    auto sldo_cb = SetLocalDescriptionObserverCb::Create([this](webrtc::RTCError error) {
        if (error.ok()) {
            RTC_LOG_F(LS_INFO) << "OK: SetLocalDescription()";
            auto desc = peer_connection_->local_description();
            SendMessage(protocolSdpMessage(desc));
            return;
        }
        RTC_LOG_F(LS_ERROR) << "failed: SetLocalDescription(): " << ToString(error.type()) << ": "
                            << error.message();
        Disconnect();
    });

    auto csdo_cb = CreateSessionDescriptionObserverCb::Create(
            [this, sldo_cb](webrtc::SessionDescriptionInterface *desc) {
                RTC_LOG_F(LS_INFO) << "SetLocalDescription()";
                // TODO check state
                peer_connection_->SetLocalDescription(sldo_cb);
            },
            [this](webrtc::RTCError error) {
                RTC_LOG_F(LS_ERROR) << "failed: SetLocalDescription(): " << ToString(error.type())
                                    << ": " << error.message();
                Disconnect();
            });

    peer_connection_->CreateAnswer(csdo_cb.get(), offer_answer_options_);
    RTC_LOG_F(LS_INFO) << "finished";
    return true;
}

void PeerConnectionObserverImpl::Disconnect()
{
    RTC_LOG_F(LS_INFO);
    SendMessage(protocolDisconnectMessage());
    disconnecting_ = true;
}

void PeerConnectionObserverImpl::Close()
{
    RTC_LOG_F(LS_INFO) << PeerConnectionStateStr(peer_connection_);
    if (!peer_connection_) {
        RTC_LOG_F(LS_WARNING) << "!peer_connection_";
        return;
    }
    disconnecting_ = true;
    signaling_thread_->Invoke<void>(RTC_FROM_HERE, [this]() {
        auto senders = peer_connection_->GetSenders();
        RTC_LOG_F(LS_INFO) << "senders.size(): " << senders.size();
        for (auto sender : senders) {
            sender->track()->set_enabled(false);
            // TODO: check WebRTC Bug: webrtc:9534
            // peer_connection_->RemoveTrack(sender);
        }
        auto receivers = peer_connection_->GetReceivers();
        RTC_LOG_F(LS_INFO) << "receivers.size(): " << receivers.size();
        for (auto receiver : receivers) {
            receiver->track()->set_enabled(false);
        }
        RTC_LOG_F(LS_INFO) << "peer_connection_->Close()";
        peer_connection_->Close();
    });
    RTC_LOG_F(LS_INFO) << "finished";
}

std::string PeerConnectionStateStr(rtc::scoped_refptr<webrtc::PeerConnectionInterface> pc)
{
    return PeerConnectionStateStr(pc.get());
}

std::string PeerConnectionStateStr(webrtc::PeerConnectionInterface *pc)
{
    if (!pc) {
        RTC_LOG_F(LS_WARNING) << "!pc";
        return "";
    }
    std::string str;
    using namespace webrtc;

    str += "\n\t";
    str += " peer_connection_state: ";
    str += std::string(PeerConnectionInterface::AsString(pc->peer_connection_state()));
    str += " local_description: ";
    str += pc->local_description() ? "Yes" : "No ";
    str += " remote_description: ";
    str += pc->remote_description() ? "Yes" : "No ";
    str += " signaling_state: ";
    str += std::string(PeerConnectionInterface::AsString(pc->signaling_state()));
    str += " ice_connection_state: ";
    str += std::string(PeerConnectionInterface::AsString(pc->ice_connection_state()));
    str += " standardized_ice_connection_state: ";
    str += std::string(PeerConnectionInterface::AsString(pc->standardized_ice_connection_state()));
    str += " ice_gathering_state: ";
    str += std::string(PeerConnectionInterface::AsString(pc->ice_gathering_state()));

    return str;
}
