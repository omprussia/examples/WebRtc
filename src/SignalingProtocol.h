// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef SIGNALINGPROTOCOL_H
#define SIGNALINGPROTOCOL_H

#include <libwebrtc/api/jsep.h>
#include <libwebrtc/rtc_base/strings/json.h>

#include <memory>
#include <string>

struct SessionDescriptionInfo
{
    std::string type;
    std::string sdp;

    static SessionDescriptionInfo fromJson(const Json::Value &jmessage);
    std::unique_ptr<webrtc::SessionDescriptionInterface> toSessionDescription() const;
};

struct IceCandidateInfo
{
    std::string sdp_mid;
    int sdp_mlineindex = 0;
    std::string sdp;
    bool endOfCandidates = false;
    bool valid = false;

    std::unique_ptr<webrtc::IceCandidateInterface> toIceCandidate() const;
};

struct ProtocolResponse
{
    SessionDescriptionInfo sessionDescriptionInfo;
    IceCandidateInfo iceCandidateInfo;
    bool disconnect = false;
    bool offer = false;
    bool answer = false;
    bool iceCandidate = false;
    bool endOfCandidates = false;
    bool error = false;
};

namespace {
// Names used for a IceCandidate JSON object.
const char kCandidateSdpMidName[] = "sdpMid";
const char kCandidateSdpMlineIndexName[] = "sdpMLineIndex";
const char kCandidateSdpName[] = "candidate";
// Names used for a SessionDescription JSON object.
const char kSessionDescriptionTypeName[] = "type";
const char kSessionDescriptionSdpName[] = "sdp";
} // namespace

ProtocolResponse protocolParseMessage(const std::string &message);
std::string protocolDisconnectMessage();
std::string protocolSdpMessage(const webrtc::SessionDescriptionInterface *desc);
std::string protocolIceCandidateMessage(const webrtc::IceCandidateInterface *candidate);

#endif // SIGNALINGPROTOCOL_H
