// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef VIDEOCONTROLLER_H
#define VIDEOCONTROLLER_H

#include <streamcamera/streamcamera.h>
#include <streamcamera/videooutput_qt5.h>

#include <QGuiApplication>
#include <QObject>
#include <QScreen>
#include <QTimer>

#include "DataChannelObserverImpl.h"
#include "Websocket.h"
#include "VideoSink.h"
#include "VideoTrackSourceHolder.h"

class VideoController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Aurora::StreamCamera::VideoOutputQt5 *videoOutputRemote READ videoOutputRemote NOTIFY
                       videoOutputRemoteChanged)
    Q_PROPERTY(Aurora::StreamCamera::VideoOutputQt5 *videoOutputLocal READ videoOutputLocal NOTIFY
                       videoOutputLocalChanged)
    Q_PROPERTY(int cameraMountAngle READ cameraMountAngle NOTIFY cameraMountAngleChanged)
    Q_PROPERTY(int screenAngle READ screenAngle WRITE setScreenAngle NOTIFY screenAngleChanged)

public:
    VideoController();
    ~VideoController();

    Aurora::StreamCamera::VideoOutputQt5 *videoOutputRemote();
    Aurora::StreamCamera::VideoOutputQt5 *videoOutputLocal();
    int cameraMountAngle();
    int screenAngle();

    Q_INVOKABLE void connect(int port, bool connect, bool call, bool disableEncryption,
                             bool echoCancellation, bool autoGainControl, bool noiseSuppression,
                             QString server, QString cameraType, QString cameraResolution,
                             QString stunList, QString forceFieldtrials, QString sendText,
                             QString sendFile);
    Q_INVOKABLE void setScreenAngle(int screenAngle);

private:
    Aurora::StreamCamera::VideoOutputQt5 m_videoOutputRemote;
    Aurora::StreamCamera::VideoOutputQt5 m_videoOutputLocal;
    std::atomic<int> m_screenAngle;
    std::atomic<int> m_cameraMountAngle;
    std::unique_ptr<PeerConnectionObserverImpl> m_peerConnectionObserver = nullptr;
    std::unique_ptr<DataChannelObserverImpl> m_dataChannelObserver = nullptr;
    std::unique_ptr<VideoTrackSourceHolder> m_videoTrackSourceLocal;
    VideoSinkCallback m_videoSinkLocal;
    VideoSinkCallback m_videoSinkRemote;
    PeerConnectionContext m_peerConnectionContext;
    Websocket m_websocket;
    Config m_config;
    QTimer m_timer;
    QTimer m_dataChannelTimer;
    QScreen *m_screen = qGuiApp->primaryScreen();

signals:
    void videoOutputRemoteChanged();
    void videoOutputLocalChanged();
    void cameraMountAngleChanged();
    void screenAngleChanged();
};

#endif // VIDEOCONTROLLER_H
