// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef CLOCK_H
#define CLOCK_H

#include <cstdint>
#include <ctime>

#include <chrono>

static inline uint64_t uptime_us()
{
    using namespace std::chrono;
    struct timespec ts = { 0, 0 };
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    return duration_cast<microseconds>(seconds{ ts.tv_sec } + nanoseconds{ ts.tv_nsec }).count();
}

static inline uint64_t realtime_us()
{
    using namespace std::chrono;
    return duration_cast<microseconds>(system_clock::now().time_since_epoch()).count();
}

#endif // CLOCK_H
