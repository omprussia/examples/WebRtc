// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef PEERCONNECTIONOBSERVERIMPL_H
#define PEERCONNECTIONOBSERVERIMPL_H

#include "SignalingProtocol.h"

#include <atomic>
#include <memory>
#include <string>
#include <vector>

#include <libwebrtc/api/peer_connection_interface.h>
#include <libwebrtc/api/task_queue/default_task_queue_factory.h>
#include <libwebrtc/pc/video_track_source.h>
#include <libwebrtc/rtc_base/task_utils/pending_task_safety_flag.h>
#include <libwebrtc/rtc_base/thread.h>

namespace {
const char kAudioLabel[] = "audio_label";
const char kVideoLabel[] = "video_label";
const char kStreamId[] = "stream_id";
} // namespace

class PeerConnectionObserverImpl : public webrtc::PeerConnectionObserver
{
    // CreateOffer and CreateAnswer callback interface
    class CreateSessionDescriptionObserverCb : public webrtc::CreateSessionDescriptionObserver
    {
    public:
        using success_func_t = std::function<void(webrtc::SessionDescriptionInterface *)>;
        using failure_func_t = std::function<void(webrtc::RTCError)>;

        static rtc::scoped_refptr<CreateSessionDescriptionObserverCb>
        Create(success_func_t onSuccess, failure_func_t onFailure)
        {
            return rtc::make_ref_counted<CreateSessionDescriptionObserverCb>(onSuccess, onFailure);
        }

        CreateSessionDescriptionObserverCb(success_func_t onSuccess, failure_func_t onFailure)
            : onSuccess_(onSuccess), onFailure_(onFailure)
        {
        }

        ~CreateSessionDescriptionObserverCb() override = default;

        void OnSuccess(webrtc::SessionDescriptionInterface *desc) override
        {
            RTC_LOG_F(LS_INFO);
            onSuccess_(desc);
        }
        void OnFailure(webrtc::RTCError error) override
        {
            RTC_LOG_F(LS_INFO);
            onFailure_(error);
        }

        success_func_t onSuccess_ = [](webrtc::SessionDescriptionInterface *desc) {};
        failure_func_t onFailure_ = [](webrtc::RTCError) {};
    };

    // SetLocalDescription() callback interface
    class SetLocalDescriptionObserverCb : public webrtc::SetLocalDescriptionObserverInterface
    {
    public:
        using complete_func_t = std::function<void(webrtc::RTCError)>;

        static rtc::scoped_refptr<SetLocalDescriptionObserverCb> Create(complete_func_t onComplete)
        {
            return rtc::make_ref_counted<SetLocalDescriptionObserverCb>(onComplete);
        }

        SetLocalDescriptionObserverCb(complete_func_t onComplete) : onComplete_(onComplete) { }

        ~SetLocalDescriptionObserverCb() override = default;

        // On success, `error.ok()` is true.
        void OnSetLocalDescriptionComplete(webrtc::RTCError error) override { onComplete_(error); }

        complete_func_t onComplete_ = [](webrtc::RTCError) {};
    };

    // SetRemoteDescription() callback interface
    class SetRemoteDescriptionObserverCb : public webrtc::SetRemoteDescriptionObserverInterface
    {
    public:
        using complete_func_t = std::function<void(webrtc::RTCError)>;

        static rtc::scoped_refptr<SetRemoteDescriptionObserverCb> Create(complete_func_t onComplete)
        {
            return rtc::make_ref_counted<SetRemoteDescriptionObserverCb>(onComplete);
        }

        SetRemoteDescriptionObserverCb(complete_func_t onComplete) : onComplete_(onComplete) { }

        ~SetRemoteDescriptionObserverCb() override = default;

        // On success, `error.ok()` is true.
        void OnSetRemoteDescriptionComplete(webrtc::RTCError error) override { onComplete_(error); }

        complete_func_t onComplete_ = [](webrtc::RTCError) {};
    };

public:
    PeerConnectionObserverImpl(rtc::Thread *signaling_thread,
                               rtc::VideoSinkInterface<webrtc::VideoFrame> *video_sink_remote);
    ~PeerConnectionObserverImpl() override;

public:
    //
    // PeerConnectionObserver implementation
    //
    void OnSignalingChange(webrtc::PeerConnectionInterface::SignalingState new_state) override;
    void OnAddTrack(
            rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver,
            const std::vector<rtc::scoped_refptr<webrtc::MediaStreamInterface>> &streams) override;
    void OnRemoveTrack(rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver) override;
    void OnDataChannel(rtc::scoped_refptr<webrtc::DataChannelInterface> channel) override;
    void OnRenegotiationNeeded() override;
    void
    OnIceConnectionChange(webrtc::PeerConnectionInterface::IceConnectionState new_state) override;
    void
    OnIceGatheringChange(webrtc::PeerConnectionInterface::IceGatheringState new_state) override;
    void OnIceCandidate(const webrtc::IceCandidateInterface *candidate) override;
    void OnIceConnectionReceivingChange(bool receiving) override;
    void OnIceCandidateError(const std::string &address, int port, const std::string &url,
                             int error_code, const std::string &error_text) override;
    void
    OnConnectionChange(webrtc::PeerConnectionInterface::PeerConnectionState new_state) override;
    void OnStandardizedIceConnectionChange(
            webrtc::PeerConnectionInterface::IceConnectionState new_state) override;

    void CreateOffer();
    void CreateAnswer();
    bool CreateAnswerDo();
    void SetRemoteDescription(SessionDescriptionInfo desc);
    void AddIceCandidate(const IceCandidateInfo &candidate);
    void Disconnect();
    void Close();

    void SendMessage(const std::string &message);

public:
    rtc::Thread *signaling_thread_;
    std::unique_ptr<webrtc::ScopedTaskSafety> signaling_thread_safety_ = nullptr;
    rtc::VideoSinkInterface<webrtc::VideoFrame> *video_sink_remote_;
    cricket::AudioOptions audio_options_;

    webrtc::PeerConnectionInterface::RTCOfferAnswerOptions offer_answer_options_ = {};
    rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection_;
    std::atomic<bool> disconnecting_ = ATOMIC_VAR_INIT(false);

    std::vector<IceCandidateInfo> candidate_list_remote_;

    std::function<void(webrtc::PeerConnectionInterface::PeerConnectionState state)>
            state_change_cb_ = [](webrtc::PeerConnectionInterface::PeerConnectionState state) {};

    std::function<void(std::string msg)> message_sender_ = [](std::string message) {
        RTC_LOG_F(LS_INFO) << "message not sent";
    };

    std::function<void(rtc::scoped_refptr<webrtc::DataChannelInterface> channel)> ondatachannel =
            [](rtc::scoped_refptr<webrtc::DataChannelInterface> channel) {
                RTC_LOG_F(LS_INFO) << "ondatachannel";
            };
};

std::string PeerConnectionStateStr(webrtc::PeerConnectionInterface *pc);
std::string PeerConnectionStateStr(rtc::scoped_refptr<webrtc::PeerConnectionInterface> pc);

#endif // PEERCONNECTIONOBSERVERIMPL_H
