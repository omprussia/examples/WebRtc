// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef VIDEOSINK_H
#define VIDEOSINK_H

#include <libwebrtc/api/media_stream_interface.h>
#include <libwebrtc/api/video/i420_buffer.h>
#include <libwebrtc/api/video/video_frame.h>
#include <libwebrtc/api/video/video_frame_buffer.h>
#include <libwebrtc/api/video/video_sink_interface.h>
#include <libwebrtc/rtc_base/logging.h>

#include <functional>
#include <mutex>

class VideoSinkCallback : public rtc::VideoSinkInterface<webrtc::VideoFrame>
{
public:
    ~VideoSinkCallback() override { RTC_LOG_F(LS_INFO); }

    void OnFrame(const webrtc::VideoFrame &videoFrame) override { OnFrameCallback(&videoFrame); }

    void OnDiscardedFrame() override { RTC_LOG_F(LS_INFO); }

    // Called on the network thread when video constraints change.
    void OnConstraintsChanged(const webrtc::VideoTrackSourceConstraints &constraints) override
    {
        RTC_LOG_F(LS_INFO) << "";
    }

    std::function<void(const webrtc::VideoFrame *videoFrame)> OnFrameCallback =
            [](const webrtc::VideoFrame *videoFrame) {};
};

#endif // VIDEOSINK_H
