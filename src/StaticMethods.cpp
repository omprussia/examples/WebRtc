// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include <libwebrtc/api/scoped_refptr.h>
#include <libwebrtc/aurora/VideoSourceStreamCamera.h>
#include <libwebrtc/rtc_base/checks.h>
#include <libwebrtc/rtc_base/ref_counted_object.h>
#include <libwebrtc/rtc_base/socket_address.h>
#include <libwebrtc/rtc_base/system/rtc_export.h>
#include <libwebrtc/third_party/libyuv/include/libyuv.h>

#include <sstream>
#include <memory>
#include <atomic>
#include <cmath>

#include "VideoTrackSourceHolder.h"
#include "video/VideoTrackSourceV4L2.h"
#include "video/FrameSourceWithBufferCallback.h"
#include "video/VideoSourcePeriodic.h"

#include "Clock.h"

#include "StaticMethods.h"

void drawFakeVideo(webrtc::I420Buffer &buffer)
{
    static int square_x_ = 0;
    static int square_y_ = 0;
    int width = buffer.width();
    int height = buffer.height();
    memset(buffer.MutableDataY(), 127, height * buffer.StrideY());
    memset(buffer.MutableDataU(), 127, buffer.ChromaHeight() * buffer.StrideU());
    memset(buffer.MutableDataV(), 127, buffer.ChromaHeight() * buffer.StrideV());

    const int square_size = 20;
    if (square_x_ >= width - square_size - 1 || square_x_ < 0)
        square_x_ = 0;
    if (square_y_ >= height - square_size - 1 || square_y_ < 0)
        square_y_ = 0;
    libyuv::I420Rect(buffer.MutableDataY(), buffer.StrideY(), buffer.MutableDataU(),
                     buffer.StrideU(), buffer.MutableDataV(), buffer.StrideV(), square_x_,
                     square_y_, square_size, square_size, 255, 10, 20);
    square_x_ += 10;
    square_y_ += 10;

    const int square_cnt = 10;
    int x_incr = width / (square_cnt + 1);
    for (size_t cnt = 0; cnt < square_cnt; cnt++) {
        libyuv::I420Rect(buffer.MutableDataY(), buffer.StrideY(), buffer.MutableDataU(),
                         buffer.StrideU(), buffer.MutableDataV(), buffer.StrideV(),
                         x_incr / 2 + x_incr * cnt, height / 2, square_size, square_size, 255, 10,
                         20);
    }

    size_t seconds_cnt = ((uptime_us() / 1000000) % 10) + 1;
    for (size_t cnt = 0; cnt < seconds_cnt; cnt++) {
        libyuv::I420Rect(buffer.MutableDataY(), buffer.StrideY(), buffer.MutableDataU(),
                         buffer.StrideU(), buffer.MutableDataV(), buffer.StrideV(),
                         x_incr / 2 + x_incr * cnt, height / 2, square_size, square_size, 76, 84,
                         255);
    }
}

rtc::scoped_refptr<webrtc::VideoTrackSource> StaticMethods::createVideoTrackSourceLocal(
        std::string type, std::string resolution, std::atomic<int> *cameraMountAngle,
        std::function<int()> encoderRotationAngleFunc = []() { return 0; })
{
    rtc::scoped_refptr<webrtc::VideoTrackSource> video_track_source = nullptr;
    *cameraMountAngle = 0;

    int width = 640;
    int height = 480;
    if (resolution != "auto") {
        if (!resolutionParse(resolution, width, height)) {
            RTC_LOG_F(LS_INFO) << "failed: resolution_parse(" << resolution;
            return nullptr;
        }
    }

    int mode = 0; // auto
    if (type == "v4l2")
        mode = 1;
    else if (type == "front" || type == "rear")
        mode = 2;
    else if (type == "fake")
        mode = 3;

    RTC_LOG_F(LS_INFO) << type << ":" << resolution << " mode: " << mode << ":" << width << "x"
                       << height;
    switch (mode) {
    case 0:
    case 1:
        video_track_source = VideoTrackSourceV4L2::Create(width, height);
        if (type != "auto")
            return video_track_source;
        else if (video_track_source)
            return video_track_source;
    case 2: {
        Aurora::StreamCamera::CameraFacing facing = Aurora::StreamCamera::CameraFacing::Front;
        if (type == "rear")
            facing = Aurora::StreamCamera::CameraFacing::Rear;

        auto video_source = std::make_shared<VideoSourceStreamCamera>();
        video_track_source =
                rtc::make_ref_counted<VideoTrackSourceImpl>(video_source, false /* remote */);

        video_source->SetCameraMode(facing, width, height);
        video_source->CaptureStart();
        *cameraMountAngle = video_source->CameraMountAngle();
        RTC_LOG_F(LS_INFO) << "cameraMountAngle :" << cameraMountAngle;

        if (type != "auto")
            return video_track_source;
        else if (video_track_source)
            return video_track_source;
    }
    case 3:
    default: {
        auto frame_source = std::make_shared<FrameSourceWithBufferCallback>(width, height);
        frame_source->on_frame_callback_ = drawFakeVideo;
        auto video_source = std::make_shared<VideoSourcePeriodic>(frame_source, 30 /*fps*/);
        video_track_source =
                rtc::make_ref_counted<VideoTrackSourceImpl>(video_source, false /* remote */);
    }
    }
    return video_track_source;
}

QUrl StaticMethods::getServerUrl(std::string server, int port)
{
    QString url_str(server.c_str());
    if (!url_str.startsWith("ws://"))
        url_str = "ws://" + url_str;
    QUrl url(url_str);
    if (url.port() == -1)
        url.setPort(port);
    return url;
}

Config StaticMethods::createConfig(std::string server, int port, bool connect, bool call,
                                   bool disable_encryption, std::string stun_list,
                                   std::string camera_type, std::string camera_resolution,
                                   std::string send_text, std::string send_file)
{
    Config config{};
    config.serverUrl = StaticMethods::getServerUrl(server, port);
    config.connect = connect;
    config.call = call;
    config.disableEncryption = disable_encryption;

    std::vector<rtc::SocketAddress> server_addresses;
    std::istringstream stun_list_stream(stun_list);
    std::string stun_str;

    while (getline(stun_list_stream, stun_str, ',')) {
        rtc::SocketAddress addr;
        if (!addr.FromString(stun_str)) {
            continue;
        }
        StunTurnServer stun{};
        stun.uri = "stun:";
        stun.uri += stun_str;
        config.stunTurnServerList.push_back(stun);
    }

    config.camera_type = camera_type;
    config.camera_resolution = camera_resolution;
    config.send_text = send_text;
    config.send_file = send_file;

    return config;
}

bool StaticMethods::resolutionParse(const std::string &resolution, int &width, int &height)
{
    int cnt = sscanf(resolution.c_str(), "%ix%i", &width, &height);
    if (cnt != 2)
        return false;
    return true;
}
