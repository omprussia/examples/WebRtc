// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "datachannelProtocol.h"

#include <cassert>

#include <vector>

namespace datachannelProtocol {

constexpr size_t chunk_size = 100000;
using chunk_list_t = std::vector<chunk_t>;

bool send_text(const std::string &text, std::function<bool(const std::string &)> sender)
{
    Json::StyledWriter writer;
    Json::Value jmessage;
    jmessage["type"] = "text";
    jmessage["encoding"] = "text";
    jmessage["text"] = text;
    std::string json = writer.write(jmessage);
    return sender(json);
}

bool send_text(DataChannelObserverImpl *dco, const std::string &text)
{
    return send_text(text, [dco](const std::string &json) {
        RTC_LOG_F(LS_INFO) << json; // TODO remove
        return dco->send(json);
    });
}

chunk_list_t data_to_chunk_list(const uint8_t *data, size_t size)
{
    chunk_list_t list;
    for (size_t idx = 0; idx < size; idx += chunk_size) {
        if (idx + chunk_size < size)
            list.emplace_back(chunk_t(data + idx, data + idx + chunk_size));
        else
            list.emplace_back(chunk_t(data + idx, data + size));
    }
    return list;
}

bool send_file_data(const char *file_name, const uint8_t *data, size_t size,
                    std::function<bool(const std::string &)> sender)
{
    RTC_LOG_F(LS_INFO);
    Json::StyledWriter writer;
    chunk_list_t chunk_list = data_to_chunk_list(data, size);
    for (size_t idx = 0; idx < chunk_list.size(); idx++) {
        auto chunk = chunk_list[idx];
        std::string base64;
        rtc::Base64::EncodeFromArray(chunk.data(), chunk.size(), &base64);
        Json::Value jmessage;
        jmessage["type"] = "file";
        jmessage["file_name"] = file_name;
        jmessage["encoding"] = "base64";
        jmessage["chunk_idx"] = idx;
        jmessage["chunk_count"] = chunk_list.size();
        jmessage["chunk"] = base64;
        std::string json = writer.write(jmessage);
        if (!sender(json))
            return false;
    }
    return true;
}

bool send_file_data(DataChannelObserverImpl *dco, const char *file_name, const uint8_t *data,
                    size_t size)
{
    return send_file_data(file_name, data, size, [dco](const std::string &json) {
        RTC_LOG_F(LS_INFO) << json; // TODO remove
        return dco->send(json);
    });
}

bool send_file_data(DataChannelObserverImpl *dco, const std::string &file_name,
                    const std::vector<uint8_t> &data)
{
    return send_file_data(dco, file_name.c_str(), data.data(), data.size());
}

Json::Value messageToJson(const std::string message)
{
    Json::Reader reader;
    Json::Value jmessage;
    if (!reader.parse(message, jmessage)) {
        RTC_LOG_F(LS_WARNING) << "failed: Json::Reader::parse()" << message;
        return jmessage;
    }
    return jmessage;
}

bool parseText(const Json::Value &jmessage, const std::string &message,
               std::function<void(std::string)> text_cb)
{
    if (!jmessage.isMember("text")) {
        RTC_LOG_F(LS_WARNING) << "failed: no \"text\" in message: " << message;
        return false;
    }
    text_cb(jmessage["text"].asString());

    return true;
}

bool parseFile(const Json::Value &jmessage, const std::string &message,
               std::function<void(file_chunk_info_t)> file_chunk_cb)
{
    const char *keys[] = { "file_name", "encoding", "chunk_idx", "chunk_count", "chunk" };
    for (const char *key : keys) {
        if (!jmessage.isMember(key)) {
            RTC_LOG_F(LS_WARNING) << "failed: no \"" << key << "\" in message: " << message;
            return false;
        }
    }
    file_chunk_info_t chunk_info;
    chunk_info.file_name = jmessage["file_name"].asString();
    chunk_info.chunk_count = jmessage["chunk_count"].asUInt();
    chunk_info.chunk_idx = jmessage["chunk_idx"].asUInt();
    std::string chunk_base64 = jmessage["chunk"].asString();
    if (!rtc::Base64::DecodeFromArray(chunk_base64.c_str(), chunk_base64.size(),
                                      rtc::Base64::DO_LAX /*flags*/, &chunk_info.chunk,
                                      nullptr /*data_used*/)) {
        RTC_LOG_F(LS_WARNING) << "failed: DecodeFromArray() in message: " << message;
        return false;
    }
    file_chunk_cb(chunk_info);

    return true;
}

bool parseMessage(const std::string &message, std::function<void(std::string)> text_cb,
                  std::function<void(file_chunk_info_t)> file_chunk_cb)
{
    Json::Value jmessage = messageToJson(message);
    if (jmessage.empty()) {
        RTC_LOG_F(LS_WARNING) << "failed: jmessage.empty()";
        return false;
    }
    std::string type;
    if (!jmessage.isMember("type")) {
        RTC_LOG_F(LS_WARNING) << "failed: no \"type\" in message: " << message;
        return false;
    }

    type = jmessage["type"].asString();
    if (type == "text")
        return parseText(jmessage, message, text_cb);
    else if (type == "file")
        return parseFile(jmessage, message, file_chunk_cb);

    RTC_LOG_F(LS_WARNING) << "failed: unknown type: " << type;
    return false;
}

} // namespace datachannelProtocol

// #define DATACHANNELPROTOCOL_TEST
#ifdef DATACHANNELPROTOCOL_TEST

int main()
{
    using namespace datachannelProtocol;

    std::string text = "Hello";
    std::string text_json;
    assert(send_text(text, [&text_json](const std::string &json) {
        RTC_LOG_F(LS_INFO) << json;
        text_json = json;
        return true;
    }));
    std::string text_from_msg;
    assert(parseMessage(
            text_json, [&text_from_msg](const std::string &text_) { text_from_msg = text_; },
            [](file_chunk_info_t) { assert(false); }));

    assert(text_from_msg == text);

    std::vector<uint8_t> data1;
    for (size_t idx = 0; idx < 256; idx++)
        data1.push_back(idx);

    std::vector<std::string> json_list;
    assert(send_file_data("zzz_file_name", data1.data(), data1.size(),
                          [&json_list](const std::string &json) {
                              RTC_LOG_F(LS_INFO) << json;
                              json_list.push_back(json);
                              return true;
                          }));

    std::vector<uint8_t> data2;
    for (auto json : json_list) {
        assert(parseMessage(
                json, [&text_json](const std::string &text_from_msg) { assert(false); },
                [&data2](file_chunk_info_t chunk_info) {
                    RTC_LOG_F(LS_INFO) << chunk_info.chunk_idx;
                    data2.insert(data2.end(), chunk_info.chunk.begin(), chunk_info.chunk.end());
                }));
    }

    assert(data1 == data2);

    RTC_LOG_F(LS_INFO) << "--OK--";
}

#endif // DATACHANNELPROTOCOL_TEST
