// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "Websocket.h"

#include <cassert>
#include <chrono>
#include <string>

#include <QAbstractSocket>
#include <QCoreApplication>
#include <QString>
#include <QWebSocket>
#include <QWebSocketServer>

#include "libwebrtc/rtc_base/logging.h"

Websocket::Websocket(QObject *parent) : QObject(parent) { }

#define S1(x) #x
#define S2(x) S1(x)
#define S3(x, y) S1(x) ":" S1(y)
#define LOCATION __FILE__ ":" S2(__LINE__)

bool Websocket::server(uint16_t port)
{
    wsReset(nullptr);
    wsServerReset(new QWebSocketServer(__PRETTY_FUNCTION__, QWebSocketServer::NonSecureMode));

    auto versionList = m_ws_server->supportedVersions();
    for (auto version : versionList)
        RTC_LOG_F(LS_INFO) << "version: " << version;

    QObject::connect(m_ws_server, &QWebSocketServer::acceptError,
                     [this](QAbstractSocket::SocketError error) {
                         RTC_LOG_F(LS_ERROR) << "m_ws_server: acceptError";
                         disconnected_on_error(LOCATION);
                     });
    QObject::connect(m_ws_server, &QWebSocketServer::closed, [this]() {
        RTC_LOG_F(LS_INFO) << "m_ws_server: closed";
        disconnected_emit(LOCATION);
    });
    QObject::connect(m_ws_server, &QWebSocketServer::newConnection, [this]() {
        RTC_LOG_F(LS_INFO) << "m_ws_server: newConnection";
        wsReset(m_ws_server->nextPendingConnection());
    });
    QObject::connect(m_ws_server, &QWebSocketServer::originAuthenticationRequired,
                     [this](QWebSocketCorsAuthenticator *authenticator) {
                         RTC_LOG_F(LS_INFO) << "m_ws_server: originAuthenticationRequired";
                     });
    QObject::connect(m_ws_server, &QWebSocketServer::peerVerifyError,
                     [this](const QSslError &error) {
                         RTC_LOG_F(LS_ERROR) << "m_ws_server: peerVerifyError";
                         disconnected_on_error(LOCATION);
                     });
    QObject::connect(m_ws_server, &QWebSocketServer::serverError,
                     [this](QWebSocketProtocol::CloseCode closeCode) {
                         RTC_LOG_F(LS_ERROR) << "m_ws_server: serverError: "
                                             << m_ws_server->errorString().toStdString();
                         disconnected_on_error(LOCATION);
                     });
    QObject::connect(m_ws_server, &QWebSocketServer::sslErrors,
                     [this](const QList<QSslError> &errors) {
                         RTC_LOG_F(LS_ERROR) << "m_ws_server: sslErrors";
                         disconnected_on_error(LOCATION);
                     });

    m_port = port;
    RTC_LOG_F(LS_INFO) << m_port;
    return m_ws_server->listen(QHostAddress::Any, m_port);
}

void Websocket::client(const QUrl &url)
{
    RTC_LOG_F(LS_INFO) << url.toString().toStdString();
    wsServerReset(nullptr);
    wsReset(new QWebSocket(__PRETTY_FUNCTION__));
    m_url = url;
    RTC_DCHECK(QMetaObject::invokeMethod(m_ws, "open", Qt::QueuedConnection, Q_ARG(QUrl, m_url)));
}

// slot
void Websocket::connectServer(const QString &url, bool connect)
{
    if (connect)
        client(url);
    else
        close();
}

void Websocket::close()
{
    RTC_LOG_F(LS_INFO);
    wsReset(nullptr);
    wsServerReset(nullptr);
}

void Websocket::sendMessage(QString message)
{
    if (!m_ws)
        return;

    RTC_DCHECK(QMetaObject::invokeMethod(this, "sendMessageSlot", Qt::QueuedConnection,
                                         Q_ARG(QString, QString(message))));
}

void Websocket::sendMessageSlot(QString message)
{
    if (!m_ws)
        return;
    m_ws->sendTextMessage(message);
}

void Websocket::wsReset(QWebSocket *ws)
{
    if (m_ws) {
        m_ws->close();
        m_ws->deleteLater();
        m_ws = nullptr;
    }
    if (ws == nullptr)
        return;
    m_ws = ws;

    QObject::connect(m_ws, &QWebSocket::connected, [this]() {
        RTC_LOG_F(LS_INFO) << "m_ws: connected";
        m_connected = true;
        Q_EMIT connected();
    });
    QObject::connect(m_ws, &QWebSocket::disconnected, [this]() {
        RTC_LOG_F(LS_INFO) << "m_ws: disconnected";
        m_connected = false;
        Q_EMIT disconnected();
    });
    QObject::connect(m_ws, &QWebSocket::textMessageReceived, [this](const QString &message) {
        Q_EMIT textMessageReceived(message.toStdString());
    });

    QObject::connect(
            m_ws,
            static_cast<void (QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error),
            [this](QAbstractSocket::SocketError error) {
                RTC_LOG_F(LS_ERROR) << "m_ws: error:" << m_ws->errorString().toStdString();
                disconnected_on_error(LOCATION);
            });
    QObject::connect(m_ws, &QWebSocket::sslErrors, [this](const QList<QSslError> &errors) {
        RTC_LOG_F(LS_ERROR) << "m_ws: sslErrors";
        for (auto error : errors)
            RTC_LOG_F(LS_ERROR) << "\t" << error.errorString().toStdString();
        disconnected_on_error(LOCATION);
    });

    if (isServer()) {
        m_connected = true;
        Q_EMIT connected();
    }
}

void Websocket::wsServerReset(QWebSocketServer *ws_server)
{
    if (m_ws_server) {
        m_ws_server->close();
        m_ws_server->deleteLater();
        m_ws_server = nullptr;
    }

    if (ws_server)
        m_ws_server = ws_server;
}

bool Websocket::isServer()
{
    return m_ws_server != nullptr;
}

void Websocket::disconnected_on_error(const char *log_message)
{
    disconnected_emit("");
}

void Websocket::disconnected_emit(const char *log_message)
{
    m_connected = false;
    Q_EMIT disconnected();
}
