// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef STATICMETHODS_H
#define STATICMETHODS_H

#include <libwebrtc/api/scoped_refptr.h>
#include <libwebrtc/api/video/i420_buffer.h>
#include <libwebrtc/pc/video_track_source.h>

#include <QUrl>

#include "Config.h"

namespace StaticMethods {
QUrl getServerUrl(std::string server, int port);
Config createConfig(std::string server, int port, bool connect, bool call, bool disable_encryption,
                    std::string stun_list, std::string camera_type, std::string camera_resolution,
                    std::string send_text, std::string send_file);
bool resolutionParse(const std::string &resolution, int &width, int &height);
rtc::scoped_refptr<webrtc::VideoTrackSource>
createVideoTrackSourceLocal(std::string type, std::string resolution,
                            std::atomic<int> *cameraMountAngle,
                            std::function<int()> encoderRotationAngleFunc);
} // namespace StaticMethods

#endif // STATICMETHODS_H
