// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "SignalingProtocol.h"

#include "libwebrtc/api/candidate.h"
#include "libwebrtc/rtc_base/checks.h"
#include "libwebrtc/rtc_base/logging.h"
#include "libwebrtc/rtc_base/ref_counted_object.h"
#include "libwebrtc/rtc_base/strings/json.h"

#include <memory>
#include <string>

Json::Value messageToJson(const std::string message)
{
    Json::Reader reader;
    Json::Value jmessage;
    if (!reader.parse(message, jmessage)) {
        RTC_LOG_F(LS_WARNING) << "failed: Json::Reader::parse()" << message;
        return jmessage;
    }
    return jmessage;
}

std::string jsonTypeStr(const Json::Value &jmessage)
{
    std::string str;
    rtc::GetStringFromJsonObject(jmessage, kSessionDescriptionTypeName, &str);
    return str;
}

std::string jsonCandidateStr(const Json::Value &jmessage)
{
    std::string str;
    rtc::GetStringFromJsonObject(jmessage, kCandidateSdpName, &str);
    return str;
}

SessionDescriptionInfo SessionDescriptionInfo::fromJson(const Json::Value &jmessage)
{
    SessionDescriptionInfo info{};
    std::string type_str = jsonTypeStr(jmessage);
    if (type_str.empty()) {
        RTC_LOG_F(LS_WARNING) << "type_str.empty()";
        return info;
    }
    std::string sdp;
    if (!rtc::GetStringFromJsonObject(jmessage, kSessionDescriptionSdpName, &sdp)) {
        RTC_LOG_F(LS_WARNING) << "No SDP in message";
        return info;
    }

    info.type = type_str;
    info.sdp = sdp;

    return info;
}

std::unique_ptr<webrtc::SessionDescriptionInterface>
SessionDescriptionInfo::toSessionDescription() const
{
    absl::optional<webrtc::SdpType> type_maybe = webrtc::SdpTypeFromString(type);
    if (!type_maybe) {
        RTC_LOG_F(LS_ERROR) << "Unknown SDP type: " << type;
        return nullptr;
    }

    webrtc::SdpParseError error;
    webrtc::SdpType type = *type_maybe;
    std::unique_ptr<webrtc::SessionDescriptionInterface> session_description =
            webrtc::CreateSessionDescription(type, sdp, &error);
    if (!session_description) {
        RTC_LOG_F(LS_WARNING) << "SdpParseError: " << error.description;
        return nullptr;
    }
    return session_description;
}

std::unique_ptr<webrtc::IceCandidateInterface> IceCandidateInfo::toIceCandidate() const
{
    if (!valid || endOfCandidates)
        return std::unique_ptr<webrtc::IceCandidateInterface>(
                webrtc::CreateIceCandidate(sdp_mid, sdp_mlineindex, cricket::Candidate()));

    webrtc::SdpParseError error;
    std::unique_ptr<webrtc::IceCandidateInterface> candidate(
            webrtc::CreateIceCandidate(sdp_mid, sdp_mlineindex, sdp, &error));
    if (!candidate.get()) {
        RTC_LOG_F(LS_WARNING) << "failed: webrtc::CreateIceCandidate(): " << error.description;
        return nullptr;
    }
    auto addr_str = candidate->candidate().address().ToString();
    if (addr_str.find(".local:") != std::string::npos) {
        RTC_LOG_F(LS_WARNING) << "drop candidate with mDNS address: " << addr_str;
        return nullptr;
    }
    return candidate;
}

IceCandidateInfo jsonParseCandidate(const Json::Value &jmessage)
{
    IceCandidateInfo ret{ .valid = false };

    std::string sdp_mid;
    int sdp_mlineindex = 0;
    std::string sdp;

    if (!rtc::GetStringFromJsonObject(jmessage, kCandidateSdpMidName, &sdp_mid)
        || !rtc::GetIntFromJsonObject(jmessage, kCandidateSdpMlineIndexName, &sdp_mlineindex)
        || !rtc::GetStringFromJsonObject(jmessage, kCandidateSdpName, &sdp)) {
        RTC_LOG_F(LS_INFO) << kCandidateSdpMidName << " or " << kCandidateSdpMlineIndexName
                           << " or " << kCandidateSdpName << " not found in json";
        return ret;
    }

    ret = { .sdp_mid = sdp_mid, .sdp_mlineindex = sdp_mlineindex, .sdp = sdp, .valid = true };

    if (sdp.empty()) {
        // https://w3c.github.io/webrtc-pc/#idl-def-rtcpeerconnection
        // If candidate.candidate is an empty string, process candidate
        // as an end-of-candidates indication for the corresponding media
        // description and ICE candidate generation.
        // FireFox sends end-of-candidates, Chromium does not
        // https://bugs.chromium.org/p/chromium/issues/detail?id=935898
        RTC_LOG_F(LS_INFO) << "end-of-candidates";
        ret.endOfCandidates = true;
    }

    return ret;
}

ProtocolResponse protocolParseMessage(const std::string &message)
{
    ProtocolResponse resp{};
    auto jmessage = messageToJson(message);
    if (jmessage.empty()) {
        resp.error = true;
        RTC_LOG_F(LS_WARNING) << "jmessage.empty()";
        return resp;
    }

    std::string type_str = jsonTypeStr(jmessage);
    if (type_str.empty()) {
        IceCandidateInfo iceInfo = jsonParseCandidate(jmessage);
        resp.endOfCandidates = iceInfo.endOfCandidates;
        if (iceInfo.toIceCandidate()) {
            resp.iceCandidate = true;
            resp.iceCandidateInfo = iceInfo;
        }
        // drop candidate with mDNS address
        if (!resp.endOfCandidates && !resp.iceCandidate)
            resp.error = true;
        return resp;
    }

    resp.disconnect = type_str == "disconnect";
    if (resp.disconnect)
        return resp;

    // For now only SDP types "offer" and "answer" are handled
    // "pranswer" not ready
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1004510
    // TODO think about "rollback", it looks ready
    // https://stackoverflow.com/questions/61956693/webrtc-perfect-negotiation-issues
    resp.sessionDescriptionInfo = SessionDescriptionInfo::fromJson(jmessage);
    auto sdp = resp.sessionDescriptionInfo.toSessionDescription();
    if (!sdp) {
        resp.error = true;
        return resp;
    }
    webrtc::SdpType sdpType = sdp->GetType();
    if (sdpType == webrtc::SdpType::kOffer)
        resp.offer = true;
    else if (sdpType == webrtc::SdpType::kAnswer)
        resp.answer = true;
    else {
        RTC_LOG_F(LS_WARNING) << "can not handle SDP type: " << type_str;
        resp.error = true;
        return resp;
    }

    return resp;
}

std::string protocolDisconnectMessage()
{
    Json::StyledWriter writer;
    Json::Value jmessage;
    jmessage[kSessionDescriptionTypeName] = "disconnect";
    return writer.write(jmessage);
}

std::string protocolSdpMessage(const webrtc::SessionDescriptionInterface *desc)
{
    std::string sdp;
    desc->ToString(&sdp);
    Json::StyledWriter writer;
    Json::Value jmessage;
    jmessage[kSessionDescriptionTypeName] = webrtc::SdpTypeToString(desc->GetType());
    jmessage[kSessionDescriptionSdpName] = sdp;
    return writer.write(jmessage);
}

std::string protocolIceCandidateMessage(const webrtc::IceCandidateInterface *candidate)
{
    Json::StyledWriter writer;
    Json::Value jmessage;

    jmessage[kCandidateSdpMidName] = candidate->sdp_mid();
    jmessage[kCandidateSdpMlineIndexName] = candidate->sdp_mline_index();
    std::string sdp;
    if (!candidate->ToString(&sdp)) {
        RTC_LOG_F(LS_ERROR) << "failed: candidate->ToString()";
        return "";
    }
    jmessage[kCandidateSdpName] = sdp;
    return writer.write(jmessage);
}
