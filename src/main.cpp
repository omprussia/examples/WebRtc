// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include <auroraapp.h>
#include <QtQml>
#include <QGuiApplication>
#include <QQuickView>

#include "VideoController.h"

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("WebrtcExamples"));

    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    qRegisterMetaType<Aurora::StreamCamera::VideoOutputQt5 *>();
    qmlRegisterType<VideoController>("ru.auroraos.webrtc", 1, 0, "VideoController");
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/WebrtcExamples.qml")));
    view->show();

    return application->exec();
}
