// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import QtQuick.Window 2.1 as QtQuick
import QtMultimedia 5.6
import Sailfish.Silica 1.0
import ru.auroraos.webrtc 1.0

Page {
    id: page

    property int port
    property bool connect
    property bool call
    property bool disableEncryption
    property bool echoCancellation
    property bool autoGainControl
    property bool noiseSuppression
    property string server
    property string cameraType
    property string cameraResolution
    property string stunList
    property string forceFieldtrials
    property string sendText
    property string sendFile
    property var screenAngle: QtQuick.Screen.angleBetween(orientation, QtQuick.Screen.primaryOrientation)

    onOrientationChanged: {
        controller.screenAngle = Qt.binding(function () {
            return QtQuick.Screen.angleBetween(page.orientation, QtQuick.Screen.primaryOrientation);
        });
    }
    onStatusChanged: {
        if (page.status === PageStatus.Active)
            controller.connect(port, connect, call, disableEncryption, echoCancellation, autoGainControl, noiseSuppression, server, cameraType, cameraResolution, stunList, forceFieldtrials, sendText, sendFile);
    }

    allowedOrientations: Orientation.All

    VideoController {
        id: controller

        screenAngle: page.screenAngle
    }

    VideoOutput {
        id: frameI420Local

        anchors.top: parent.top
        anchors.bottom: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        objectName: "frameI420Local"
        visible: true
        orientation: {
            if (cameraType === "rear" && (page.orientation === Orientation.Landscape || page.orientation === Orientation.LandscapeInverted))
                return 180 - (page.screenAngle + controller.cameraMountAngle);
            else
                return 360 - (page.screenAngle + controller.cameraMountAngle);
        }
        source: controller.videoOutputLocal
        fillMode: VideoOutput.PreserveAspectFit
    }

    VideoOutput {
        id: frameI420Remote

        anchors.top: parent.verticalCenter
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        objectName: "frameI420Remote"
        visible: true
        source: controller.videoOutputRemote
        fillMode: VideoOutput.PreserveAspectFit
    }

}
