// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Page {
    property string sendFileName
    property string sendFilePath
    property int _defaultPort: 8881

    function connectToHost() {
        var cameraType;
        switch (cameraTypeComboBox.currentIndex) {
        case 0:
            cameraType = "front";
            break;
        case 1:
            cameraType = "rear";
            break;
        case 2:
            cameraType = "fake";
            break;
        }
        pageStack.push(Qt.resolvedUrl("VideoPage.qml"), {
                "port": parseInt(port.text),
                "connect": connectSwitch.checked,
                "call": callSwitch.checked,
                "disableEncryption": disableEncryptionSwitch.checked,
                "echoCancellation": echoCancellationSwitch.checked,
                "autoGainControl": autoGainControlSwitch.checked,
                "noiseSuppression": noiseSuppressionSwitch.checked,
                "server": server.text,
                "cameraType": cameraType,
                "cameraResolution": cameraResolution.text,
                "stunList": stunList.text,
                "forceFieldtrials": forceFieldtrials.text,
                "sendText": message.text,
                "sendFile": sendFilePath
            });
    }

    objectName: "mainPage"
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: column.height

        PullDownMenu {
            id: pullDownMenu

            MenuItem {
                onClicked: {
                    pullDownMenu.cancelBounceBack();
                    pageStack.push(filePickerPage);
                }

                text: qsTr("Select file for sending")
            }
        }

        Column {
            id: column

            anchors {
                left: parent.left
                right: parent.right
            }

            spacing: Theme.paddingLarge

            PageHeader {
                id: header

                title: qsTr("WebRTC Examples")
                extraContent.children: [
                    IconButton {
                        onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))

                        anchors.verticalCenter: parent.verticalCenter

                        icon.source: "image://theme/icon-m-about"
                    }
                ]
            }

            Column {
                id: textColumn

                anchors {
                    left: parent.left
                    right: parent.right
                }

                spacing: Theme.paddingMedium

                TextSwitch {
                    id: connectSwitch

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Connect")
                }

                TextSwitch {
                    id: callSwitch

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Call")
                }

                TextSwitch {
                    id: disableEncryptionSwitch

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Disable encryption")
                }

                TextSwitch {
                    id: echoCancellationSwitch

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Echo cancellation")
                }

                TextSwitch {
                    id: autoGainControlSwitch

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Auto volume control")
                }

                TextSwitch {
                    id: noiseSuppressionSwitch

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Noise suppression")
                }

                ComboBox {
                    id: cameraTypeComboBox

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    label: qsTr("Camera type:")
                    menu: ContextMenu {
                        MenuItem {
                            text: qsTr("Front")
                        }

                        MenuItem {
                            text: qsTr("Rear")
                        }

                        MenuItem {
                            text: qsTr("Fake")
                        }
                    }
                }

                TextField {
                    id: server

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    label: qsTr("Server")
                    placeholderText: qsTr("Server")
                    text: "ws://127.0.0.1"

                    EnterKey.iconSource: "image://theme/icon-m-enter-next"
                    EnterKey.onClicked: port.focus = true
                }

                TextField {
                    id: port

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    label: qsTr("Port")
                    placeholderText: qsTr("Port")
                    text: _defaultPort
                    inputMethodHints: Qt.ImhDigitsOnly
                    validator: RegExpValidator {
                        regExp: /^[0-9]+$/
                    }

                    EnterKey.iconSource: "image://theme/icon-m-enter-next"
                    EnterKey.onClicked: cameraResolution.focus = true
                }

                TextField {
                    id: cameraResolution

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    label: qsTr("Camera resolution")
                    placeholderText: qsTr("Camera resolution")
                    text: "640x480"

                    EnterKey.iconSource: "image://theme/icon-m-enter-next"
                    EnterKey.onClicked: stunList.focus = true
                }

                TextField {
                    id: stunList

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    label: qsTr("Stun list")
                    placeholderText: qsTr("Stun list")

                    EnterKey.iconSource: "image://theme/icon-m-enter-next"
                    EnterKey.onClicked: forceFieldtrials.focus = true
                }

                TextField {
                    id: forceFieldtrials

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    label: qsTr("Force fieldtrials")
                    placeholderText: qsTr("Force fieldtrials")

                    EnterKey.iconSource: "image://theme/icon-m-enter-next"
                    EnterKey.onClicked: message.focus = true
                }

                TextField {
                    id: message

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    label: qsTr("Message")
                    placeholderText: qsTr("Message")

                    EnterKey.onClicked: {
                        message.focus = false;
                        connectToHost();
                    }
                }

                Label {
                    anchors {
                        left: parent.left
                        right: parent.right
                        leftMargin: Theme.paddingLarge
                    }

                    text: qsTr("Sending file") + ": " + sendFileName
                    width: parent.width
                    wrapMode: Text.Wrap
                }

                Button {
                    id: connectButton

                    onClicked: connectToHost()

                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: Theme.horizontalPageMargin
                    }

                    text: qsTr("Connect")
                }
            }
        }

        Component {
            id: filePickerPage

            FilePickerPage {
                onSelectedContentPropertiesChanged: {
                    sendFileName = selectedContentProperties.fileName;
                    sendFilePath = selectedContentProperties.filePath;
                }

                title: qsTr("Choose file")
            }
        }
    }
}
